package dao

import groovy.sql.Sql
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

/**
 * Simple class which implements the needed methods to perform CRUD (Create, Read, Update, Delete) operations over the following vbox database tables:<br>
 *     - CONFIGLETS<br>
 *     - PARAMETERS<br>
 */
@SuppressWarnings("all")
class sdDAO {

    /**
     * sdDAO class logger
     */
    private static Logger log = LogManager.getLogger(sdDAO.class.getName())

    //General Connection (DEV,PRE,PRO)
    /**
     * Unique connection to the Service Director database in the whole application received as a parameter.<br>
     * This parameter is mandatory.
     */
    def connection

    /**
     * Sql object instantiated with the connection received in the constructor method and unique for all the methods in this class
     */
    def sql

    sdDAO(connectionParameter) {
        connection = connectionParameter
        sql = new Sql(connection)
    }

    /**
     * Method which makes a SQL query to retrieve all the <b>vbox</b> configlets that are available in the Service Director database
     * @return GroovyRowResult list containing all the configlets available in the internal Service Director database or an empty list is no configlet is found.
     */
    def getvbx_em_configlets_data() {
        log.info("ENTERING. 'getvbx_em_configlets_data' method. Class " + sdDAO.class.getName())
        //def sql = new Sql(connection)
        if (this.sql == null) {
            this.sql = new Sql(connection)
        }
        //def result = sql.rows("select CONFIGLET_NAME,CONFIGLET_ID WHERE CONFIGLET_NAME = '$configletName' from VBX_EM_CONFIGLETS")
        def result = this.sql.rows("select * from VBX_EM_CONFIGLETS")
        //sql.close() Si cierro la conexión hay que instanciar una nueva cada vez
        log.info("EXITING. 'getvbx_em_configlets_data' method. Class " + sdDAO.class.getName())
        return result
    }

    /**
     * Method which makes a SQL query to retrieve all the <b>vbox</b> configlets parameters that are available in the Service Director database
     * @return GroovyRowResult list containing all the configlets parameters available in the internal Service Director database or an empty list is no configlet parameter is found.
     */
    def getvbx_em_configlets_params_data() {
        log.info("ENTERING. 'getvbx_em_configlets_params_data' method. Class " + sdDAO.class.getName())
        //def sql = new Sql(connection)
        if (this.sql == null) {
            this.sql = new Sql(connection)
        }
        //def result = sql.rows("select PARAM_NAME,PARAM_ID WHERE CONFIGLET_ID = '$configletID' from VBX_EM_CONFIGLETS_PARAMS")
        def result = this.sql.rows("select * from VBX_EM_CONFIGLETS_PARAMS")
        //sql.close() Si cierro la conexión tengo que instanciar una nueva cada vez
        log.info("EXITING. 'getvbx_em_configlets_params_data' method. Class " + sdDAO.class.getName())
        return result
    }

    /**
     * Method to retrieve only the name and the ID of all the configlets available in the internal Service Director database
     * @return GroovyRowResult list containing the name and the ID of all the configlets available in the internal Service Director database or an empty list is no configlet is found.
     */
    def getAllLocalConfiglets() {
        log.info("ENTERING. 'getAllLocalConfiglets' method. Class " + sdDAO.class.getName())
        //def sql = new Sql(connection)
        if (this.sql == null) {
            this.sql = new Sql(connection)
        }
        //def result = sql.rows("select CONFIGLET_NAME,CONFIGLET_ID WHERE CONFIGLET_NAME = '$configletName' from VBX_EM_CONFIGLETS")
        def result = this.sql.rows("select CONFIGLET_NAME,CONFIGLET_ID from VBX_EM_CONFIGLETS")
        //sql.close() Si cierro la conexión tengo que instanciar una nueva cada vez
        log.info("EXITING. 'getAllLocalConfiglets' method. Class " + sdDAO.class.getName())
        return result
    }

    /**
     * Method to retrieve all local configlet parameters of a given configlet ID available in the internal Service Director database
     * @param configletID
     * @return GroovyRowResult list containing the parameters name and ID of the given configlet ID or empty list if the configlet doesn't have any parameter in the local database
     */
    def getLocalConfigletParamsByConfigletID(String configletID) {
        log.info("ENTERING. 'getLocalConfigletParamsByConfigletID' method. Class " + sdDAO.class.getName())
        //def sql = new Sql(connection)
        if (this.sql == null) {
            this.sql = new Sql(connection)
        }
        //def result = sql.rows("select PARAM_NAME,PARAM_ID WHERE CONFIGLET_ID = '$configletID' from VBX_EM_CONFIGLETS_PARAMS")
        def result = this.sql.rows("select PARAM_NAME,PARAM_ID from VBX_EM_CONFIGLETS_PARAMS where CONFIGLET_ID = ?", [configletID])
        //sql.close() Si cierro la conexión tengo que instanciar una nueva cada vez
        log.info("EXITING. 'getLocalConfigletParamsByConfigletID' method. Class " + sdDAO.class.getName())
        return result
    }

    /**
     * Method to insert a non existing configlet in the internal Service Director database
     * @param configletName
     * @param configletID
     * @return Integer greater than zero if the insertion has been done correctly
     */
    def insertNewConfiglet(String configletName, String configletID) {
        log.info("ENTERING. 'insertNewConfiglet' method. Class " + sdDAO.class.getName())
        //def sql = new Sql(connection)
        if (this.sql == null) {
            this.sql = new Sql(connection)
        }
        //def result = sql.execute("insert into VBX_EM_CONFIGLETS (CONFIGLET_NAME,CONFIGLET_ID) values ('$configletName','$configletID')")
        def result = this.sql.executeInsert("insert into VBX_EM_CONFIGLETS (CONFIGLET_NAME,CONFIGLET_ID) values (?,?)", [configletName, configletID])
        //sql.close() Si cierro la conexión tengo que instanciar una nueva cada vez
        log.info("EXITING. 'insertNewConfiglet' method. Class " + sdDAO.class.getName())
        return result
    }

    /**
     * Method to insert a non existing configlet parameter in the internal Service Director database
     * @param paramName
     * @param paramID
     * @param configletID
     * @return Integer greater than zero if the insertion has been done correctly
     */
    def insertNewConfigletParam(String paramName, String paramID, Integer configletID) {
        log.info("ENTERING. 'insertNewConfigletParam' method. Class " + sdDAO.class.getName())
        //def sql = new Sql(connection)
        if (this.sql == null) {
            this.sql = new Sql(connection)
        }
        //def result = sql.execute("insert into VBX_EM_CONFIGLETS_PARAMS (PARAM_NAME,PARAM_ID,CONFIGLET_id) values ('$paramName','$paramID', '$configletID')")
        def result = this.sql.executeInsert("insert into VBX_EM_CONFIGLETS_PARAMS (PARAM_NAME,PARAM_ID,CONFIGLET_ID) values (?,?,?)", [paramName, paramID, configletID])
        //sql.close() Si cierro la conexión tengo que instanciar una nueva cada vez
        log.info("EXITING. 'insertNewConfigletParam' method. Class " + sdDAO.class.getName())
        return result
    }

    /**
     * Method to update a configlet ID with a new one given as a parameter
     * @param configletID
     * @param newConfigletID
     * @return Returns "1" if the field has been updated or "0" if no updates have been done
     */
    def updateConfigletID(String configletID, String newConfigletID) {
        log.info("ENTERING. 'updateConfigletID' method. Class " + sdDAO.class.getName())
        //def sql = new Sql(connection)
        if (this.sql == null) {
            this.sql = new Sql(connection)
        }
        //def result = sql.executeUpdate("update VBX_EM_CONFIGLETS set CONFIGLET_ID = $newConfigletID where CONFIGLET_NAME = '$configletName'")
        def result = this.sql.executeUpdate("update VBX_EM_CONFIGLETS set CONFIGLET_ID = ? where CONFIGLET_ID = ?", [newConfigletID, configletID])
        //sql.close() Si cierro la conexión tengo que instanciar una nueva cada vez
        log.info("EXITING. 'updateConfigletID' method. Class " + sdDAO.class.getName())
        return result
    }

    /**
     * Method to update a configlet parameter ID with a new one given as a parameter for a given configlet ID
     * @param oldParamID
     * @param newParamID
     * @param configletID
     * @return Returns "1" if the field has been updated or "0" if no updates have been done
     */
    def updateParamID(String oldParamID, String newParamID, String configletID) {
        log.info("ENTERING. 'updateParamID' method. Class " + sdDAO.class.getName())
        //def sql = new Sql(connection)
        if (this.sql == null) {
            this.sql = new Sql(connection)
        }
        //def result = sql.executeUpdate("update VBX_EM_CONFIGLETS set CONFIGLET_NAME = $name where CONFIGLET_ID = '$configletId'")
        def result = this.sql.executeUpdate("update VBX_EM_CONFIGLETS_PARAMS set PARAM_ID = ? where PARAM_ID = ? and CONFIGLET_ID = ?", [newParamID, oldParamID, configletID])
        //sql.close() Si cierro la conexión tengo que instanciar una nueva cada vez
        log.info("EXITING. 'updateParamID' method. Class " + sdDAO.class.getName())
        return result
    }

    /**
     * Method to update simultaniuously the ID of the configlet and the parameter ID in the parameters table in the Service Director database
     * @param paramName Parameter name to update
     * @param oldParamID The old parameter ID
     * @param newParamID The new parameter ID
     * @param oldConfigletID The old configlet ID
     * @param newConfigletID The new configlet ID
     * @return Returns "1" if the field has been updated or "0" if no updates have been done
     */
    def updateParamAndConfigID(String paramName, String oldParamID, String newParamID, String oldConfigletID, String newConfigletID) {
        log.info("ENTERING. 'updateParamAndConfigID' method. Class " + sdDAO.class.getName())
        //def sql = new Sql(connection)
        if (this.sql == null) {
            this.sql = new Sql(connection)
        }
        //def result = sql.executeUpdate("update VBX_EM_CONFIGLETS set CONFIGLET_NAME = $name where CONFIGLET_ID = '$configletId'")
        def result = this.sql.executeUpdate("update VBX_EM_CONFIGLETS_PARAMS set PARAM_ID = ?, CONFIGLET_ID = ? where PARAM_NAME = ? and CONFIGLET_ID = ?", [newParamID, newConfigletID, paramName, oldConfigletID])
        //sql.close() Si cierro la conexión tengo que instanciar una nueva cada vez
        log.info("EXITING. 'updateParamAndConfigID' method. Class " + sdDAO.class.getName())
        return result
    }

    /**
     * Method to set the param ID to "-1" indicating that it has to be deleted because it doesn't exist in the Juniper Element Manager
     * @param configletParamID String param ID to set its value to "-1"
     * @return Returns "1" if the field has been updated or "0" if no updates have been done
     */
    def markToDeleteConfigletParamDueToNonExistingConfigletParam(String configletParamID) {
        log.info("ENTERING. 'markToDeleteConfigletParamDueToNonExistingConfigletParamForConfigletID' method. Class " + sdDAO.class.getName())
        //def sql = new Sql(connection)
        if (this.sql == null) {
            this.sql = new Sql(connection)
        }
        //def result = sql.executeUpdate("update VBX_EM_CONFIGLETS set CONFIGLET_NAME = $name where CONFIGLET_ID = '$configletId'")
        def result = this.sql.executeUpdate("update VBX_EM_CONFIGLETS_PARAMS set PARAM_ID = ? where PARAM_ID = ?", ["-1", configletParamID])
        //sql.close() Si cierro la conexión tengo que instanciar una nueva cada vez
        log.info("EXITING. 'markToDeleteConfigletParamDueToNonExistingConfigletParamForConfigletID' method. Class " + sdDAO.class.getName())
        return result
    }

    /**
     * Method to set the configlet ID column to "-1" indicating that it has to be deleted because it doesn't exist in the Juniper Element Manager
     * @param configletName String name of the local configlet to delete
     * @return Returns "1" if the field has been updated or "0" if no updates have been done
     */
    def markToDeleteLocalConfiglet(String configletName) {
        log.info("ENTERING. 'markToDeleteLocalConfiglet' method. Class " + sdDAO.class.getName())
        //def sql = new Sql(connection)
        if (this.sql == null) {
            this.sql = new Sql(connection)
        }
        //def result = sql.executeUpdate("update VBX_EM_CONFIGLETS set CONFIGLET_NAME = $name where CONFIGLET_ID = '$configletId'")
        def result = this.sql.executeUpdate("update VBX_EM_CONFIGLETS set CONFIGLET_ID = ? where CONFIGLET_NAME = ?", ["-1", configletName])
        //sql.close() Si cierro la conexión tengo que instanciar una nueva cada vez
        log.info("EXITING. 'markToDeleteLocalConfiglet' method. Class " + sdDAO.class.getName())
        return result
    }

    /**
     * Method to set the configlet ID and parameter ID in the parameter table to "-1" indicating that they have to be deleted because they don't exist in the Juniper Element Manager
     * @param configletId Integer ID of the configlet to mark to delete
     * @return Returns "1" if the field has been updated or "0" if no updates have been done
     */
    def markToDeleteConfigletParamsForNonExistingConfigletID(Integer configletId) {
        log.info("ENTERING. 'markToDeleteConfigletParamForNonExistingConfigletID' method. Class " + sdDAO.class.getName())
        //def sql = new Sql(connection)
        if (this.sql == null) {
            this.sql = new Sql(connection)
        }
        //def result = sql.executeUpdate("update VBX_EM_CONFIGLETS set CONFIGLET_NAME = $name where CONFIGLET_ID = '$configletId'")
        def result = this.sql.executeUpdate("update VBX_EM_CONFIGLETS_PARAMS set CONFIGLET_ID = ? , PARAM_ID = ? where CONFIGLET_ID = ?", ["-1", "-1", configletId])
        //sql.close() Si cierro la conexión tengo que instanciar una nueva cada vez
        log.info("EXITING. 'markToDeleteConfigletParamForNonExistingConfigletID' method. Class " + sdDAO.class.getName())
        return result
    }

    /**
     * Method to delete all those parameters have been marked previously with a "-1"
     * @return Returns "1" if the field has been deleted or "0" if no deletions have been done
     */
    def deleteMarkedConfigletsParams() {
        log.info("ENTERING. 'deleteMarkedConfigletsParams' method. Class " + sdDAO.class.getName())
        //def sql = new Sql(connection)
        if (this.sql == null) {
            this.sql = new Sql(connection)
        }
        //def result = sql.executeUpdate("update VBX_EM_CONFIGLETS set CONFIGLET_NAME = $name where CONFIGLET_ID = '$configletId'")
        def result = this.sql.execute("delete from VBX_EM_CONFIGLETS_PARAMS where PARAM_ID = ?", ["-1"])
        //sql.close() Si cierro la conexión tengo que instanciar una nueva cada vez
        log.info("EXITING. 'deleteMarkedConfigletsParams' method. Class " + sdDAO.class.getName())
        return result
    }

    /**
     * Method to delete all those configlets have been marked previously with a "-1"
     * @return Returns "1" if the field has been deleted or "0" if no deletions have been done
     */
    def deleteMarkedConfiglets() {
        log.info("ENTERING. 'deleteMarkedConfigletsParams' method. Class " + sdDAO.class.getName())
        //def sql = new Sql(connection)
        if (this.sql == null) {
            this.sql = new Sql(connection)
        }
        //def result = sql.executeUpdate("update VBX_EM_CONFIGLETS set CONFIGLET_NAME = $name where CONFIGLET_ID = '$configletId'")
        def result = this.sql.execute("delete from VBX_EM_CONFIGLETS where CONFIGLET_ID = ?", ["-1"])
        //sql.close() Si cierro la conexión tengo que instanciar una nueva cada vez
        log.info("EXITING. 'deleteMarkedConfigletsParams' method. Class " + sdDAO.class.getName())
        return result
    }

    /**
     * Method to update a configlet ID of in the parameters table for a new one
     * @param oldConfigID String of the non updated configlet ID
     * @param newConfigID String of the new configlet ID
     * @return Returns "1" if the field has been updated or "0" if no updates have been done
     */
    def updateConfigletIDInParametersTable(Integer oldConfigID, Integer newConfigID) {
        log.info("ENTERING. 'updateConfigletIDInParametersTable' method. Class " + sdDAO.class.getName())

        if (this.sql == null) {
            this.sql = new Sql(connection)
        }

        def oldConfigIDString = oldConfigID.toString()
        def newConfigIDString = newConfigID.toString()

        //log.info("Actualizando configlet con IDs: " + oldConfigID + " con el nuevo: " + newConfigID)
        log.info("Updating configlet with IDs:" + oldConfigID + " with the new value: " + newConfigID)
        //List updateCount = this.sql.rows("""update VBX_EM_CONFIGLETS_PARAMS set CONFIGLET_ID = $newConfigIDString where CONFIGLET_ID = $oldConfigIDString""")
        def updateCount = this.sql.executeUpdate("""update VBX_EM_CONFIGLETS_PARAMS set CONFIGLET_ID = $newConfigIDString where CONFIGLET_ID = $oldConfigIDString""")
        if (updateCount != null) {
            if (updateCount > 0) {
                //log.info("Actualizados " + updateCount.length + " IDs de configlet")
                log.info("Updated " + updateCount.length + " configlet IDs")
            } else {
                //log.info("No se ha actualizado ningún ID de configlet")
                og.info("No configlet ID has been updated")
            }
        } else {
            //log.error("No se ha ejecutado el batch de los IDs de configlets correctamente debido a un error")
            //log.info("Realizando ROLLBACK")
            log.error("The batch of configlet IDs was not executed correctly due to an error")
            log.info("Performing ROLLBACK")
            sql.getDataSource().getConnection().rollback()
            //log.info("Se ha realizado ROLLBACK de la transacción")
            log.info("Transaction ROLLBACK has been performed")
        }
        //sql.close() Si cierro la conexión tengo que instanciar una nueva cada vez
        log.info("EXITING. 'updateConfigletIDInParametersTable' method. Class " + sdDAO.class.getName())
        return updateCount
    }

    def closeSDConnection() {
        this.sql.close()
    }
}