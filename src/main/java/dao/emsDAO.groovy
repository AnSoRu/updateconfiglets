package dao

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.google.gson.internal.LinkedTreeMap
import com.google.gson.reflect.TypeToken
import org.apache.commons.codec.binary.Base64

import org.apache.http.HttpResponse
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpRequestBase
import org.apache.http.conn.ssl.SSLConnectionSocketFactory
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import util.EncryptDecryptUtil

import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLContext
import javax.net.ssl.X509TrustManager
import java.lang.reflect.Type
import java.security.KeyManagementException
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.security.cert.X509Certificate

/**
 * Simple class which implements the needed methods to perform different operations to retrieve
 * the configlets from the Juniper Element Manager System.
 */
@SuppressWarnings("all")
class emsDAO{

    /**
     *emsDAO class logger
     */
    private static Logger log = LogManager.getLogger(emsDAO.class.getName())
    private static final String GET = "GET"
    /**
     *IP address of the Juniper Element Manager System.<br>
     *This parameter is mandatory.
     */
    private String EMSIPAddress
    /**
     *IP port where the Juniper Element Manager System is running.<br>
     *This parameter is mandatory.
     */
    private String EMSIPPort

    /**
     * Method to set the Juniper Element Manager System IP address.
     * @param EMSIPAddress
     */
    void setEMSIPAddress(String EMSIPAddress) {
        this.EMSIPAddress = EMSIPAddress
    }
    /**
     * Method to set the Juniper Element Manager System IP port.
     * @param EMSIPPort
     */
    void setEMSIPPort(String EMSIPPort) {
        this.EMSIPPort = EMSIPPort
    }
    /**
     * Method to retrieve the established Juniper Element Manager System IP address.
     * @return Juniper Element Manager System established IP address
     */
    String getEMSIPAddress() {
        return EMSIPAddress
    }
    /**
     * Method to retrieve the established Juniper Element Manager System IP port.
     * @return Juniper Element Manager System IP port
     */
    String getEMSIPPort() {
        return EMSIPPort
    }

    /**
     * Method to build a HttpClient initializing a SSLContext and SSLSocket to communicates with the EMS.
     * @return HttpClient inizialized with a SSLContext
     * @throws KeyManagementException
     * @throws NoSuchAlgorithmException
     */
    private static HttpClient initClient() throws KeyManagementException, NoSuchAlgorithmException {
        log.info("ENTERING. 'initClient' method. Class " + emsDAO.class.getName())
        SSLContext sslcontext = getMySSLContext()
        SSLConnectionSocketFactory factory = new SSLConnectionSocketFactory(sslcontext, SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER)
        log.info("EXITING. 'initClient' method. Class " + emsDAO.class.getName())
        return HttpClientBuilder.create().setSSLSocketFactory(factory).build()
    }

    /**
     * Method to instantiate a SSLContext trusting in all the Juniper Element Manager certificates
     * @return SSLContext trusting all the Juniper Element Manager certificates
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     */
    private static SSLContext getMySSLContext() throws NoSuchAlgorithmException, KeyManagementException {
        X509TrustManager x509TrustManagerVariable = new X509TrustManager() {
            @Override
             X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[1]
            }

             void checkClientTrusted(X509Certificate[] certs, String authType) {
                log.info("checkClientTrusted =============")
            }

             void checkServerTrusted(X509Certificate[] certs, String authType) {
                log.info("checkServerTrusted =============")
            }
        }
        log.info("ENTERING. 'getMySSLContext' method. Class " + emsDAO.class.getName())
        X509TrustManager [] arrayTrustManager = new X509TrustManager[1]
        arrayTrustManager[0] = x509TrustManagerVariable
        SSLContext sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null,arrayTrustManager,new SecureRandom())
        log.info("EXITING. 'getMySSLContext' method. Class " + emsDAO.class.getName())
        return sslContext
    }

    /**
     * Method to set a <b>mandatory</b> Authorization user header and specific http Accept request headers depending on the kind of request sent to the Element Manager API.
     * @param request A HttpRequestBase to set up with the Authorization and Accept headers
     * @param user EMS <b>encrypted</b> username
     * @param password EMS <b>encrypted</b> password
     * @param httpMethod By default is a GET method
     * @param url URL String to which make the request
     * @throws Exception if the request cannot be built
     */
    private static void addHeaders(HttpRequestBase request, String user, String password, String httpMethod, String url) throws Exception {
        log.info("ENTERING. 'addHeaders' method. Class " + emsDAO.class.getName())
        request.addHeader("Authorization", getAuthorization(user, password))
        switch (httpMethod) {
            case GET:
                if (url.matches(".+/api/space/configuration-management/cli-configlets/")){
                    request.addHeader("Accept","application/vnd.net.juniper.space.configuration-management.cli-configlets+json;version=3")
                }else if(url.matches(".+/api/space/configuration-management/cli-configlets/.+/cli-configlet-params")){
                    request.addHeader("Accept","application/vnd.net.juniper.space.configuration-management.cli-configlet-params+json;version=3")
                }else if(url.matches(".+/api/space/debuglog-management/")){
                    request.addHeader("Accept","application/vnd.net.juniper.space.debuglog-management+json;version=1")
                }
                break
            default:
                log.error("Operation not defined")
        }
        log.info("EXITING. 'addHeaders' method. Class " + emsDAO.class.getName())
    }

    /**
     * Method to get a default HttpGET request, built with the URL, user and password parameters
     * @param url URL to which make the request in String format
     * @param user EMS <b>encrypted</b> username
     * @param password EMS <b>encrypted</b> password
     * @return HttpGet request which points to the received URL
     * @throws Exception if the request cannot be built
     */
    private static HttpGet getRequestGET(String url, String user, String password) throws Exception {
        log.info("ENTERING. 'getRequestGET' method. Class " + emsDAO.class.getName())
        HttpRequestBase request = new HttpGet(url)
        addHeaders(request, user, password, "GET", url)
        log.info("EXITING. 'getRequestGET' method. Class " + emsDAO.class.getName())
        return (HttpGet) request
    }

    /**
     * Method to set the EMS authorization with the user and password
     * @param user EMS <b>encrypted</b> username
     * @param password EMS <b>encrypted</b> password
     * @return String with the authorization in EMS format
     * @throws Exception
     */
    static String getAuthorization(String user, String password) throws Exception {
        log.info("ENTERING. 'getAuthorization' method. Class " + emsDAO.class.getName())
        String authorization = user + ":" + password
        byte[] encodedBytes = Base64.encodeBase64(authorization.getBytes())
        authorization = "Basic " + new String(encodedBytes)
        log.info("EXITING. 'getAuthorization' method. Class " + emsDAO.class.getName())
        return authorization
    }

    /**
     * Custom method to execute over a preconfigured HttpClient a GET request and receive a parsed response from the EMS.
     * @param httpClient HttpClient preconfigured in the initClient() method of this class
     * @param request Http GET request to execute
     * @return HashMap containing a Http code response and the parsed response in JSON format or NULL if an error occurs.
     */
    private static HashMap<String, String> executeGET(HttpClient httpClient, HttpGet request) {
        log.info("ENTERING. 'executeGET' method. Class " + emsDAO.class.getName())
        String retCode
        HashMap<String, String> result = new HashMap<>()
        try {
            log.info("ExecuteGET:" +request.getURI())
            HttpResponse response = httpClient.execute(request)
            retCode = String.valueOf(response.getStatusLine().getStatusCode())
            if (response.getStatusLine().getStatusCode() != 200) {
                if (response.getStatusLine().getStatusCode() == 204) { //No Content
                    log.info("Warning : HTTP error code : " + response.getStatusLine().getStatusCode() + ":" + response.getStatusLine().getReasonPhrase())
                    return result
                } else {
                    BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())))
                    String errorMessage = br.readLine()
                    log.info("Failed : HTTP error code : " + response.getStatusLine().getStatusCode() + ":" + response.getStatusLine().getReasonPhrase() + " -> " + errorMessage)
                    throw new Exception(errorMessage)
                }
            }
            BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())))
            String output = br.readLine()
            log.info("ExecuteGET:" +request.getURI() + " Response: " + response.getStatusLine().getStatusCode()  + output)
            result.put("jsonOut", output)
            result.put("retCode", retCode)
            log.info("EXITING. 'executeGET' method. Class " + emsDAO.class.getName())
            return result
        } catch (Exception e) {
            log.error(e.printStackTrace())
            //log.error("No se ha ejecutado correctamente el GET")
            log.error("The GET operation was not executed correctly")
            log.info("EXITING. 'executeGET' method. Class " + emsDAO.class.getName())
            return null
        }
    }

    /**
     * Method to convert an input JSON string into a HashMap
     * @param jsonIn JSON string to convert
     * @return HashMap of the JSON input string
     */
    private static ArrayList<HashMap<String, String>> convertjSON2HashMap(String jsonIn) {
        log.info("ENTERING. 'convertjSON2HashMap' method. Class " + emsDAO.class.getName())
        ArrayList<HashMap<String, String>> result
        Gson gson = new Gson()
        Type type2 = new TypeToken<ArrayList<HashMap<String, Object>>>() {
        }.getType()
        try{
            result = gson.fromJson(jsonIn, type2)
        } catch(JsonSyntaxException ignored){
            result = gson.fromJson("["+jsonIn+"]", type2)
        }
        log.info("EXITING. 'convertjSON2HashMap' method. Class " + emsDAO.class.getName())
        return result
    }

    /**
     * Method to check the connectivity to the Juniper Element Manager.<br>
     * This method makes a GET request to a specific URL of the Element Manager and checks if the response code is 200.<br>
     * If the returned code is not 200 then it disconnects the communication and returns a NULL object.<br>
     * If the returned code is 200 then returns a "true".
     * @return Boolean variable "true" or "false" depending on the response of the Juniper Element Manager
     */
    boolean checkEMSConnectivity(){
        log.info("ENTERING. 'checkEMSConnectivity' method. Class " + emsDAO.class.getName())
        HttpGet request = getRequestGET("https://"+ this.EMSIPAddress + ":" + this.EMSIPPort + "/api/space/debuglog-management/", EncryptDecryptUtil.decrypt("PZcd2ckEC9XOJ9mJLI7KCA=="),EncryptDecryptUtil.decrypt("3bDrpGqfUwmCRZTr1HFAvw=="))
        HttpClient httpClient
        HashMap invocationResult
        HttpsURLConnection conn
        boolean result = false
        try {
            conn = new URL("https://"+ this.EMSIPAddress + ":" + this.EMSIPPort + "/api/space/debuglog-management/").openConnection()
            httpClient = initClient()
            try {
                invocationResult = executeGET(httpClient, request)
                if (invocationResult != null) {
                    ArrayList<HashMap<String, String>> jsonResultConverted = convertjSONHashMap(invocationResult.get("jsonOut"))
                    HashMap<String, String> hashMap = jsonResultConverted.get(0)
                    if (hashMap.get("debuglog-management") != null) {
                        //log.info("Se puede establecer una conexión con el EMS")
                        log.info("A connection to the EMS can be established")
                        result = true
                    }
                } else {
                    //log.error("No se ha ejecutado correctamente el GET debuglog-management")
                    //log.info("Cerrando conexión con el EMS")
                    log.error("The debuglog-management GET has not been executed correctly")
                    log.info("Closing connection with the EMS")
                    conn.disconnect()
                    log.info("EXITING. 'checkEMSConnectivity' method. Class " + emsDAO.class.getName())
                    return null
                }
            }catch(Exception e){
                log.error(e.printStackTrace())
                //log.info("Cerrando conexión con el EMS")
                log.info("Closing connection with the EMS")
                conn.disconnect()
                log.info("EXITING. 'checkEMSConnectivity' method. Class " + emsDAO.class.getName())
                return null
            }
        }catch (Exception e) {
            log.error(e.printStackTrace())
            log.info("EXITING. 'checkEMSConnectivity' method. Class " + emsDAO.class.getName())
            return null
        }
        //log.info("Cerrando conexión con el EMS")
        log.info("Closing connection with the EMS")
        conn.disconnect()
        log.info("EXITING. 'checkEMSConnectivity' method. Class " + emsDAO.class.getName())
        return result
    }


    /**
     * Method to retrieve only the <b>vbox</b> configlets that are stored in the Juniper Element Manager.<br>
     * This method makes a GET request to a specific URL of the Element Manager and checks if the response is not NULL.<br>
     * It stores all the downloaded <b>vbox</b> configlets into a HashMap using the name of the configlet as a key and a unique
     * identifyer generated byt Juniper as the value.<br>
     * If an error ocurs when retrieving the configlets a NULL object is returned.
     * @return Non empty HashMap with the <b>vbox</b> configlets; an empty HashMap if no configlet is found or a NULL object if an error occurs.
     */
    HashMap getAllConfiglets() {
        log.info("ENTERING. 'getAllConfiglets' method. Class " + emsDAO.class.getName())
        HttpGet request = getRequestGET("https://"+ this.EMSIPAddress + ":" + this.EMSIPPort + "/api/space/configuration-management/cli-configlets/",EncryptDecryptUtil.decrypt("PZcd2ckEC9XOJ9mJLI7KCA=="),EncryptDecryptUtil.decrypt("3bDrpGqfUwmCRZTr1HFAvw=="))
        HttpClient httpClient
        HashMap<String, String> invocationResult
        HttpsURLConnection conn
        HashMap<String,Integer> configletsMap = new HashMap<>()
        try {
            conn = new URL("https://"+ this.EMSIPAddress + ":" + this.EMSIPPort + "/api/space/configuration-management/cli-configlets/").openConnection()
            httpClient = initClient()
            try{
                invocationResult = executeGET(httpClient, request)
                if(invocationResult != null){
                    if(!invocationResult.isEmpty()){
                    ArrayList<HashMap<String,String>> jsonResultConverted = convertjSON2HashMap(invocationResult.get("jsonOut"))
                    HashMap<String,String> hashMap = jsonResultConverted.get(0)
                    //ID de configlet Nombre de Configlet
                    LinkedTreeMap ltm = hashMap.get("cli-configlets") //Este tiene 3 claves "@uri","@total" y "cli-configlet" que es un ArrayList
                    ArrayList cliConfiglets = ltm.get("cli-configlet")
                    cliConfiglets.stream().forEach({ configlet ->
                        String name = configlet.getAt("name")
                        if(name.contains("vbox")){
                            int id = configlet.getAt("id")
                            configletsMap.put(name,id)
                        }
                    })
                    }else{
                        //log.info("No se ha recuperado ningún configlet.")
                        log.info("No configlet has been recovered.")
                    }
                }else{
                    //log.error("No se ha ejecutado correctamente el GET cli-configlets")
                    //log.info("Cerrando conexión con el EMS")
                    log.error("The GET cli-configlets was not executed correctly")
                    log.info("Closing connection with the EMS")
                    conn.disconnect()
                    log.info("EXITING. 'getAllConfiglets' method. Class " + emsDAO.class.getName())
                    return null
                }
            }catch(Exception e){
                log.error(e.printStackTrace())
                //log.info("Cerrando conexión con el EMS")
                log.info("Closing connection with the EMS")
                conn.disconnect()
                log.info("EXITING. 'getAllConfiglets' method. Class " + emsDAO.class.getName())
                return null
            }
        } catch (Exception e) {
            log.error(e.printStackTrace())
            log.info("EXITING. 'getAllConfiglets' method. Class " + emsDAO.class.getName())
            return null
        }
        //log.info("Cerrando conexión con el EMS")
        log.info("Closing connection with the EMS")
        conn.disconnect()
        log.info("EXITING. 'getAllConfiglets' method. Class " + emsDAO.class.getName())
        return configletsMap
    }

    /**
     * Method to retrieve all the parameters associated with the configlet ID received in the parameter.<br>
     * This method makes a GET request to a specific URL of the Element Manager and checks if the response code is 200.<br>
     * @param configletId The configlet ID from which to retrieve all its parameters
     * @return Non empty HashMap with the <b>vbox</b> configlets parameters; an empty HashMap if no configlet parameter is found for the given configlet ID or a NULL object if an error occurs.
     */
    HashMap getAllParamsForConfiglet(String configletId){
        log.info("ENTERING. 'getAllParamsForConfiglet' method. Class " + emsDAO.class.getName())
        HttpGet request = getRequestGET("https://"+ this.EMSIPAddress + ":" + this.EMSIPPort + "/api/space/configuration-management/cli-configlets/"+configletId+"/cli-configlet-params",EncryptDecryptUtil.decrypt("PZcd2ckEC9XOJ9mJLI7KCA=="),EncryptDecryptUtil.decrypt("3bDrpGqfUwmCRZTr1HFAvw=="))
        HttpClient httpClient
        HashMap<String, String> invocationResult
        HttpsURLConnection conn
        HashMap<String,String> paramsMap = new HashMap<>() //<param-name>:<param-id>
        try{
            conn = new URL("https://"+ this.EMSIPAddress + ":" + this.EMSIPPort + "/api/space/configuration-management/cli-configlets/"+configletId+"/cli-configlet-params").openConnection()
            httpClient = initClient()
            invocationResult = executeGET(httpClient, request)
            if(invocationResult != null){
                ArrayList<HashMap<String,String>> jsonResultConverted = convertjSON2HashMap(invocationResult.get("jsonOut"))
                HashMap<String,String> hashMap = jsonResultConverted.get(0)
                LinkedTreeMap cliParams = hashMap.get("cli-configlet-params")
                int total = Integer.valueOf(cliParams.get("@total"))
                if(total > 1){
                    ArrayList cliConfigletParamsList = cliParams.get("cli-configlet-param")
                    cliConfigletParamsList.stream().forEach({parameter ->
                        String configletParamName = parameter.getAt("parameter")
                        int idConfigletParam = parameter.getAt("id")
                        String idConfigletParamString = String.valueOf(idConfigletParam)
                        paramsMap.put(configletParamName,idConfigletParamString)
                    })
                }else if(total == 1){
                    LinkedTreeMap ltm = cliParams.get("cli-configlet-param")
                    int idConfigletParam = ltm.get("id")
                    String idConfigletParamString = String.valueOf(idConfigletParam)
                    String configletParamName = ltm.get("parameter")
                    paramsMap.put(configletParamName,idConfigletParamString)
                }
            }else{
                //log.error("No se ha ejecutado correctamente el GET cli-configlet-params")
                //log.info("Cerrando conexión con el EMS")
                log.error("The GET cli-configlet-params operation was not executed correctly")
                log.info("Closing connection with the EMS")
                conn.disconnect()
                log.info("EXITING. 'getAllParamsForConfiglet' method. Class " + emsDAO.class.getName())
                return null
            }
        }catch(Exception e){
            log.error(e.printStackTrace())
            log.info("EXITING. 'getAllConfiglets' method. Class " + emsDAO.class.getName())
            return null
        }
        //log.info("Closing connection with the EMS")
        log.info("Closing connection with the EMS")
        conn.disconnect()
        log.info("EXITING. 'getAllParamsForConfiglet' method. Class " + emsDAO.class.getName())
        return paramsMap
    }
}