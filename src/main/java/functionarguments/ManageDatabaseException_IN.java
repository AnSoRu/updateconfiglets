package functionarguments;

import dao.sdDAO;

import java.io.File;

public class ManageDatabaseException_IN {

    private int localConfigletID;
    private Exception e;
    private File configletsParamsBackupFile;
    private String IPDatabaseAddress;
    private String IPDatabasePort;
    private String serviceName;
    private sdDAO mySDDAO;

    public ManageDatabaseException_IN() {
    }

    public int getLocalConfigletID() {
        return localConfigletID;
    }

    public void setLocalConfigletID(int localConfigletID) {
        this.localConfigletID = localConfigletID;
    }

    public Exception getE() {
        return e;
    }

    public void setE(Exception e) {
        this.e = e;
    }

    public File getConfigletsParamsBackupFile() {
        return configletsParamsBackupFile;
    }

    public void setConfigletsParamsBackupFile(File configletsParamsBackupFile) {
        this.configletsParamsBackupFile = configletsParamsBackupFile;
    }

    public String getIPDatabaseAddress() {
        return IPDatabaseAddress;
    }

    public void setIPDatabaseAddress(String IPDatabaseAddress) {
        this.IPDatabaseAddress = IPDatabaseAddress;
    }

    public String getIPDatabasePort() {
        return IPDatabasePort;
    }

    public void setIPDatabasePort(String IPDatabasePort) {
        this.IPDatabasePort = IPDatabasePort;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public sdDAO getMySDDAO() {
        return mySDDAO;
    }

    public void setMySDDAO(sdDAO mySDDAO) {
        this.mySDDAO = mySDDAO;
    }
}
