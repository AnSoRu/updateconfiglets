package main

import connectivity.bbddSDConnectivity
import dao.emsDAO
import dao.sdDAO
import functionarguments.ManageDatabaseException_IN
import groovy.sql.GroovyRowResult
import org.apache.commons.io.FileUtils
import org.apache.logging.log4j.*
import util.ExecuteSQLFile

import java.text.SimpleDateFormat

/**
 * This class is the <b>main</b> class which receives
 */
@SuppressWarnings("all")
class UpdateConfiglets {

    /*
     Para compilar el proyecto y generar un .jar
     1) Añadir en los plugins del build del archivo pom.xml el siguiente plugin
     1.1) Indicar la clase principal a ejecutar
            <plugin>
                <artifactId>maven-assembly-plugin</artifactId>
                <configuration>
                    <archive>
                        <manifest>
                            <mainClass>[ruta en el proyecto de la clase principal a ejecutar]
                            [Ejemplo]
                            <mainClass>main.UpdateConfiglets</mainClass>
                        </manifest>
                    </archive>
                </configuration>
            </plugin>
     1) Abrir una linea de comandos
     2) Situarse en la carpeta principal del proyecto ej: \[usuario]\[workspace]\UpdateConfiglets\
     3) Ejecutar el siguiente comando mvn clean compile assembly:single
     4) Se debe de generar el siguiente archivo
     */

    private static Logger log = LogManager.getLogger(UpdateConfiglets.class.getName())

    static HashMap transformToHashMap(List<GroovyRowResult> groovyRowResults) {
        log.info("ENTERING. transformToHashMap")
        HashMap result = new HashMap()
        if (groovyRowResults != null && groovyRowResults.size() > 0) {
            for (GroovyRowResult grr : groovyRowResults) {
                String configletName = grr.getProperty("CONFIGLET_NAME")
                String configletID = grr.getProperty("CONFIGLET_ID")
                result.put(configletName, configletID)
            }
        } else {
            //log.info("Lista de entrada a transformar vacía.")
            log.info("Empty input list. No transformations made.")
        }
        log.info("EXITING. transformToHashMap")
        return result
    }

    private static void manageDatabaseException(int localConfigletID, Exception e, File configletsParamsBackupFile, String IPDatabaseAddress, String IPDatabasePort, String serviceName, sdDAO mySDDAO) {
        log.fatal("ERROR WHEN MARKING TO DELETE CONFIGLET PARAMETERS OF THE CONFIGLET WITH ID " + localConfigletID)
        log.fatal(e.printStackTrace())
        if (configletsParamsBackupFile != null) {
            String pathAux = configletsParamsBackupFile.getPath()
            //log.fatal("Ejecutando archivo de backup de los parametros de los configlets " + pathAux)
            log.fatal("Executing backup file of configlet parameters " + pathAux)
            ExecuteSQLFile executeSQLFile = new ExecuteSQLFile()
            executeSQLFile.setIPDatabaseAddress(IPDatabaseAddress)
            executeSQLFile.setIPDatabasePort(IPDatabasePort)
            if (serviceName != null && !serviceName.isEmpty()) {
                executeSQLFile.setserviceName(serviceName)
            }
            executeSQLFile.registerSQLAndExecuteSQLFile(pathAux)
            //log.info("Cerrando conexión a la base de datos")
            log.info("Closing database connection")
            mySDDAO.closeSDConnection()
            log.info("EXITING. UpdateConfiglets")
            System.exit(-1)
        } else {
            //log.info("No existe archivo de backup de parametros de configlets al no haber previamente parametros en la base de datos")
            //log.info("Cerrando conexión a la base de datos")
            log.info("There is no backup file of configlet parameters because there are no previously parameters in the database")
            log.info("Closing database connection")
            mySDDAO.closeSDConnection()
            log.info("EXITING. UpdateConfiglets")
            System.exit(-1)
        }
    }

    private static void manageDatabaseException_v2(ManageDatabaseException_IN manageDatabaseException_in) {
        log.fatal("ERROR WHEN MARKING TO DELETE CONFIGLET PARAMETERS OF THE CONFIGLET WITH ID " + manageDatabaseException_in.getLocalConfigletID())
        log.fatal(manageDatabaseException_in.getE().printStackTrace())
        if (manageDatabaseException_in.getConfigletsParamsBackupFile() != null) {
            String pathAux = manageDatabaseException_in.getConfigletsParamsBackupFile().getPath()
            //log.fatal("Ejecutando archivo de backup de los parametros de los configlets " + pathAux)
            log.fatal("Executing backup file of configlet parameters " + pathAux)
            ExecuteSQLFile executeSQLFile = new ExecuteSQLFile()
            executeSQLFile.setIPDatabaseAddress(manageDatabaseException_in.getIPDatabaseAddress())
            executeSQLFile.setIPDatabasePort(manageDatabaseException_in.getIPDatabasePort())
            def serviceName = manageDatabaseException_in.getServiceName()
            if (serviceName != null && !serviceName.isEmpty()) {
                executeSQLFile.setserviceName(serviceName)
            }
            executeSQLFile.registerSQLAndExecuteSQLFile(pathAux)
            //log.info("Cerrando conexión a la base de datos")
            log.info("Closing database connection")
            manageDatabaseException_in.getMySDDAO().closeSDConnection()
            log.info("EXITING. UpdateConfiglets")
            System.exit(-1)
        } else {
            //log.info("No existe archivo de backup de parametros de configlets al no haber previamente parametros en la base de datos")
            //log.info("Cerrando conexión a la base de datos")
            log.info("There is no backup file of configlet parameters because there are no previously parameters in the database")
            log.info("Closing database connection")
            manageDatabaseException_in.getMySDDAO().closeSDConnection()
            log.info("EXITING. UpdateConfiglets")
            System.exit(-1)
        }
    }

    static void main(String[] args) {
        //Configuracion previa de los logs
        /*URL mySource = UpdateConfiglets.class.getProtectionDomain().getCodeSource().getLocation()
        File rootFolder = new File(mySource.getPath())
        System.setProperty("app.root", rootFolder.getAbsolutePath())*/
        log.info("ENTERING. UpdateConfiglets")
        if(args.length != 4 && args.length != 5){
            /*log.error("Numero de parametros incorrectos")
            log.info("USO: ")
            log.info("main.UpdateConfiglets [Dirección IP del EMS] [Puerto EMS] [Dirección IP Base de datos de SD] [Puerto Base de datos de SD] [opcional: Nombre del servicio de la BBDD en caso de no ser la base de datos local]")
            log.info("Ejemplos:")
            log.info("main.UpdateConfiglets 127.0.0.1 22943 127.0.0.1 1521")
            log.info("main.UpdateConfiglets 127.0.0.1 22943 127.0.0.1 1521 MYSERVICE")*/
            log.error("Incorrect number of input parameters")
            log.info("HELP: ")
            log.info("main.UpdateConfiglets [EMS IP address] [EMS service port] [SD database IP address] [SD database service port] [optional: Name of the database service in case of it is not the local dabatase]")
            log.info("Examples:")
            log.info("main.UpdateConfiglets 127.0.0.1 22943 127.0.0.1 1521")
            log.info("main.UpdateConfiglets 127.0.0.1 22943 127.0.0.1 1521 MYSERVICE")
            System.exit(-1)
        }
        if (args[0] != null && !"".equals(args[0])) { //Direccion IP EMS
            if (args[1] != null && !"".equals(args[1])) { //Puerto EMS
                if (args[2] != null && !"".equals(args[2])) { //Direccion IP BBDD SD
                    if (args[3] != null && !"".equals(args[3])) { //Puerto BBDD SD
                        String serviceName
                        if(args.length > 4){
                            if(args[4] != null && !"".equals(args[4])) { //Nombre Servicio en caso de no ser local
                                serviceName = args[4]
                            }
                        }
                        String EMSIPAddress = args[0]
                        String EMSIPPort = args[1]
                        String IPDatabaseAddress = args[2]
                        String IPDatabasePort = args[3]
                        /*log.info("DIRECCION IP EMS: " + EMSIPAddress)
                        log.info("PUERTO EMS: " + EMSIPPort)
                        log.info("DIRECCION IP BBDD: " + IPDatabaseAddress)
                        log.info("PUERTO BBDD: " + IPDatabasePort)*/
                        log.info("EMS IP ADDRESS: " + EMSIPAddress)
                        log.info("EMS TCP PORT: " + EMSIPPort)
                        log.info("DATABASE IP ADDRESS: " + IPDatabaseAddress)
                        log.info("DATABASE TCP PORT: " + IPDatabasePort)
                        if(serviceName != null && !serviceName.isEmpty()){
                            log.info("NAME OF THE DATABASE SERVICE: " + serviceName)
                        }
                        def myEMSDAO = new emsDAO()
                        myEMSDAO.setEMSIPAddress(EMSIPAddress)
                        myEMSDAO.setEMSIPPort(EMSIPPort)
                        //log.info("Comprobando conexión al EMS en la IP " + myEMSDAO.getEMSIPAddress() + " y puerto " + myEMSDAO.getEMSIPPort())
                        log.info("Checking the connectivity to the EMS through the IP address " + myEMSDAO.getEMSIPAddress() + " and TCP port " + myEMSDAO.getEMSIPPort())
                        if (!myEMSDAO.checkEMSConnectivity()) {
                            //log.error("No se ha podido establecer una conexión con el EMS en la IP " + myEMSDAO.getEMSIPAddress() + " y puerto " + myEMSDAO.getEMSIPPort())
                            //log.info("EXITING. UpdateConfiglets")
                            log.error("Unable to establish a connection with the EMS over the IP address " + myEMSDAO.getEMSIPAddress() + " and TCP port " + myEMSDAO.getEMSIPPort())
                            log.info("EXITING. UpdateConfiglets")
                            System.exit(-1)
                        }
                        //log.info("Obteniendo conexión a la base de datos del SD en la IP " + IPDatabaseAddress + " y puerto " + IPDatabasePort)
                        log.info("Trying to get a Service Director database connection through the IP address " + IPDatabaseAddress + " and TCP port " + IPDatabasePort)
                        def mySDConnectivity = new bbddSDConnectivity()
                        //if (IPDatabaseAddress.equals("127.0.0.1") || IPDatabaseAddress.equals("localhost")) {
                        if (serviceName == null || serviceName.isEmpty()) {
                            mySDConnectivity.setToLocalDatabase()
                        } else {
                            mySDConnectivity.setIPDatabaseAddress(IPDatabaseAddress)
                            mySDConnectivity.setIPDatabasePort(IPDatabasePort)
                            mySDConnectivity.setserviceName(serviceName)
                            mySDConnectivity.setConnection()
                        }
                        def mySDConnection = mySDConnectivity.getConnection()
                        if (mySDConnection == null) {
                            //log.error("No se ha podido obtener una conexión a la base de datos del SD en la IP " + mySDConnectivity.getIPDatabaseAddress() + " y puerto " + mySDConnectivity.getIPDatabasePort())
                            //log.info("EXITING. UpdateConfiglets")
                            log.error("Unable to get a connection to the Service Director database through the IP address " + mySDConnectivity.getIPDatabaseAddress() + " and TCP port " + mySDConnectivity.getIPDatabasePort())
                            log.info("EXITING. UpdateConfiglets")
                            System.exit(-1)
                        }
                        def mySDDAO = new sdDAO(mySDConnection)
                        //log.info("Obtenida conexión a la base de datos del SD")
                        log.info("A connection to the Service Director database has been obtained")
                        //------------------------------------------------------------------------------------------
                        //Hacer backup de lo existente en las tablas VBX_EM_CONFIGLETS y VBX_EM_CONFIGETS_PARAMS
                        //log.info("Recuperando los datos actuales de la tabla VBX_EM_CONFIGLETS")
                        log.info("Retrieving the current data from the VBX_EM_CONFIGLETS table")
                        def backupconfiglets = mySDDAO.getvbx_em_configlets_data()
                        File configletsBackupFile = null
                        if (backupconfiglets != null && backupconfiglets.size() > 0) {
                            //log.info("Realizando back-up de los datos actuales de la tabla VBX_EM_CONFIGLETS")
                            log.info("Making a back-up of the current data of the VBX_EM_CONFIGLETS table")
                            File configletsBackupFileOriginal = new File(UpdateConfiglets.class.getClassLoader().getResource("configletsBackup.sql").toURI())
                            String renamed = UpdateConfiglets.class.getClassLoader().getResource("configletsBackup.sql").toURI().getPath()
                            String[] renamedArray = renamed.split(".sql")
                            SimpleDateFormat sdf = new SimpleDateFormat("_dd-MM-yyyy_HH-mm-ss")
                            Date dAux = new Date()
                            String formatedDate = sdf.format(dAux)
                            String name = renamedArray[0] + formatedDate + ".sql"
                            configletsBackupFile = new File(name)
                            FileUtils.copyFile(configletsBackupFileOriginal, configletsBackupFile)
                            if (configletsBackupFile != null) {
                                configletsBackupFile.append("-- ################################")
                                configletsBackupFile.append('\n')
                                configletsBackupFile.append("-- " + new Date().toString())
                                configletsBackupFile.append('\n')
                                configletsBackupFile.append("TRUNCATE TABLE VBX_EM_CONFIGLETS;")
                                configletsBackupFile.append('\n')
                                configletsBackupFile.append('\n')
                                for (GroovyRowResult grr : backupconfiglets) {
                                    String configletName = grr.getProperty("CONFIGLET_NAME")
                                    String configletID = grr.getProperty("CONFIGLET_ID")
                                    String entity = grr.getProperty("ENTITY")
                                    String action = grr.getProperty("ACTION")
                                    String step = grr.getProperty("STEP")

                                    String resultInsertInit = "insert into VBX_EM_CONFIGLETS (CONFIGLET_NAME,CONFIGLET_ID,ENTITY,ACTION,STEP) values ('"
                                    String resultInsertValues = configletName + "','" + configletID + "','" + entity + "','" + action + "','" + step
                                    String resultInsertFinal = "');"

                                    String resultInsert = resultInsertInit + resultInsertValues + resultInsertFinal
                                    configletsBackupFile.append(resultInsert)
                                    configletsBackupFile.append('\n')
                                }
                            } else {
                                //log.error("No se ha podido obtener el archivo configletsBackup.sql")
                                //log.info("Cerrando conexión a la base de datos del SD")
                                log.error("Unable to get the configletsBackup.sql file")
                                log.info("Closing the connection to the Service Director database")
                                log.info("EXITING. UpdateConfiglets")
                                mySDDAO.closeSDConnection()
                                //log.info("Se ha cerrado la conexión a la base de datos del SD")
                                System.exit(-1)
                            }
                            configletsBackupFile.append("-- ################################")
                            configletsBackupFile.append("\n")
                            //log.info("Back-up de la tabla VBX_EM_CONFIGLETS realizado con exito. Guardado en el archivo: " + configletsBackupFile.getAbsolutePath())
                            log.info("Back-up of the VBX_EM_CONFIGLETS table successfully completed. Saved to file: " + configletsBackupFile.getAbsolutePath())
                        } else if (backupconfiglets.size() == 0) {
                            //log.info("ConfigletsBackup: No existen configlets en la base de datos")
                            //log.info("No se realiza ningun backup")
                            log.info("ConfigletsBackup: There are no configlets in the database")
                            log.info("No backup is performed")
                        } else {
                            //log.error("No se ha podido obtener ninguna respuesta de la base de datos del SD")
                            //log.info("Cerrando conexión a la base de datos del SD")
                            log.error("Could not get any response from Service Director database")
                            log.info("Closing connection to the Service Director database")
                            mySDDAO.closeSDConnection()
                            log.info("EXITING. UpdateConfiglets")
                            System.exit(-1)
                        }
                        //------------------------------------------------------------------------------------------
                        //------------------------------------------------------------------------------------------
                        //log.info("Recuperando los datos actuales de la tabla VBX_EM_CONFIGLETS_PARAMS")
                        log.info("Retrieving the current data from the VBX_EM_CONFIGLETS_PARAMS table")
                        def backupconfigletsparams = mySDDAO.getvbx_em_configlets_params_data()
                        //Que estan desactualizados
                        File configletsParamsBackupFile = null
                        if (backupconfigletsparams != null && backupconfigletsparams.size() > 0) {
                            //log.info("Realizando back-up de los datos actuales de la tabla VBX_EM_CONFIGLETS_PARAMS")
                            log.info("Making a back-up of the current data of the VBX_EM_CONFIGLETS_PARAMS table")
                            File configletsParamsBackupFileOriginal = new File(UpdateConfiglets.class.getClassLoader().getResource("configletsParamsBackup.sql").toURI())
                            String renamed = UpdateConfiglets.class.getClassLoader().getResource("configletsParamsBackup.sql").toURI().getPath()
                            String[] renamedArray = renamed.split(".sql")
                            SimpleDateFormat sdf = new SimpleDateFormat("_dd-MM-yyyy_HH-mm-ss")
                            Date dAux = new Date()
                            String formatedDate = sdf.format(dAux)
                            String name = renamedArray[0] + formatedDate + ".sql"
                            configletsParamsBackupFile = new File(name)
                            FileUtils.copyFile(configletsParamsBackupFileOriginal, configletsParamsBackupFile)

                            if (configletsParamsBackupFile != null) {
                                configletsParamsBackupFile.append("-- ################################")
                                configletsParamsBackupFile.append('\n')
                                configletsParamsBackupFile.append("-- " + new Date().toString())
                                configletsParamsBackupFile.append('\n')
                                configletsParamsBackupFile.append("TRUNCATE TABLE VBX_EM_CONFIGLETS_PARAMS;")
                                configletsParamsBackupFile.append('\n')
                                configletsParamsBackupFile.append('\n')
                                for (GroovyRowResult grr : backupconfigletsparams) {
                                    String paramName = grr.getProperty("PARAM_NAME")
                                    String paramID = grr.getProperty("PARAM_ID")
                                    String configletID = grr.getProperty("CONFIGLET_ID")

                                    String resultInsertInit = "insert into VBX_EM_CONFIGLETS_PARAMS (PARAM_NAME,PARAM_ID,CONFIGLET_ID) values ('"
                                    String resultInsertValues = paramName + "','" + paramID + "','" + configletID
                                    String resultInsertFinal = "');"

                                    String resultInsert = resultInsertInit + resultInsertValues + resultInsertFinal
                                    configletsParamsBackupFile.append(resultInsert)
                                    configletsParamsBackupFile.append('\n')
                                }
                            } else {
                                //log.error("No se ha podido obtener el archivo configletsParamsBackup.sql")
                                //log.info("Cerrando conexión a la base de datos del SD")
                                log.error("Could not get file configletsParamsBackup.sql")
                                log.info("Closing connection to the Service Director database")
                                mySDDAO.closeSDConnection()
                                //log.info("Se ha cerrado la conexión a la base de datos del SD")
                                log.info("The connection to the Service Director database has been closed")
                                System.exit(-1)
                            }
                            configletsParamsBackupFile.append("-- ################################")
                            configletsParamsBackupFile.append("\n")
                            //log.info("Back-up de la tabla VBX_EM_CONFIGLETS_PARAMS realizado con exito. Guardado en el archivo: " + configletsParamsBackupFile.getAbsolutePath())
                            log.info("Back-up of the VBX_EM_CONFIGLETS_PARAMS table successfully completed. Saved to file:" + configletsParamsBackupFile.getAbsolutePath())
                        } else if (backupconfigletsparams.size() == 0) {
                            //Si no hay nada en la bbdd habrá que insertarlos
                            /*log.info("ConfigletsParamsBackup: No existen parametros de configlets en la base de datos")
                            log.info("No se realiza ningun backup de los parametros")*/
                            log.info("ConfigletsParamsBackup: There are no configlet parameters in the database")
                            log.info("No backup of the parameters is performed")
                        } else {
                            //log.error("No se ha podido obtener ninguna respuesta de la base de datos del SD")
                            //log.info("Cerrando conexión a la base de datos del SD")
                            log.error("Could not get any response from Service Director database")
                            log.info("Closing connection to the SD database")
                            mySDDAO.closeSDConnection()
                            log.info("EXITING. UpdateConfiglets")
                            System.exit(-1)
                        }
                        //------------------------------------------------------------------------------------------
                        //PASO 1: RECUPERAR TODOS LOS <configlet-name, id> actuales de la base de datos
                        //log.info("Recuperando los configlets existentes en la base de datos local")
                        log.info("Retrieving existing configlets from the local database")
                        def localConfiglets = mySDDAO.getAllLocalConfiglets() //Que estan desactualizados
                        def totalConfigletsProcessed = 0
                        if (localConfiglets != null && localConfiglets.size() > 0) {
                            //Si he llegado aqui es que hay configlets en local
                            //log.info("Se han recuperado " + localConfiglets.size() + " configlets de la base de datos local")
                            //log.info("Comprobando conexión al EMS en el puerto " + EMSIPPort)
                            log.info(localConfiglets.size() + " local database configlets have been retrieved")
                            log.info("Checking connection to the EMS in the port " + EMSIPPort)
                            if (!myEMSDAO.checkEMSConnectivity()) {
                                //log.error("No se ha podido establecer una conexión con el EMS en el puerto " + EMSIPPort)
                                //log.info("Cerrando conexión a la base de datos")
                                log.error("Unable to establish a connection with the EMS on the port " + EMSIPPort)
                                log.info("Closing database connection")
                                mySDDAO.closeSDConnection()
                                log.info("EXITING. UpdateConfiglets")
                                System.exit(-1)
                            }
                            //PASO 2: RECUPERAR TODOS LOS CONFIGLETS DEL EMS (la API no ofrece descargar por nombre los configlets)
                            //LOOP SOBRE LOS CONFIGLETS RECUPERADOS DEL EMS
                            //log.info("Recuperando desde el EMS los configlets existentes de la solución vbox")
                            log.info("Retrieving from the EMS the existing vbox solution configlets")
                            def downloadedConfiglets = myEMSDAO.getAllConfiglets() //Esto es un GroovyRowResult
                            if (downloadedConfiglets != null && downloadedConfiglets.size() > 0) {
                                def totaldownloadedConfiglets = downloadedConfiglets.size()
                                //log.info("Se han recuperado " + totaldownloadedConfiglets + " configlets desde el EMS pertenecientes a la solución vbox")
                                log.info( totaldownloadedConfiglets + " configlets have been retrieved from the EMS belonging to the vbox solution")
                                //PASO 3: COMPARAR EL ID ALMACENADO CON EL DESCARGADO
                                Set keys = downloadedConfiglets.keySet()
                                //log.info("Transformando a HashMap los configlets locales")
                                log.info("Transforming local configlets to HashMap")
                                //K = configletName V = configletID
                                HashMap transformedLocalConfigletsHashMap = transformToHashMap(localConfiglets)
                                if (transformedLocalConfigletsHashMap == null) {
                                    //log.error("Error al transformar la lista de configlets a HashMap")
                                    //log.info("Cerrando conexión a la base de datos")
                                    log.error("Error transforming the list of configlets to HashMap")
                                    log.info("Closing database connection")
                                    mySDDAO.closeSDConnection()
                                    log.info("EXITING. UpdateConfiglets")
                                    System.exit(-1)
                                }
                                //log.info("Recorriendo los configlets descargados")
                                log.info("Looping the downloaded configlets list")
                                for (String downloadedConfigletName : keys) {
                                    //Recorrer todos los configlets descargados
                                    /**
                                     * La clave que vamos a usar es el nombre del configlet (se supone que no cambia)
                                     */
                                    /**
                                     * SITUACIONES QUE PUEDEN DARSE UNICAMENTE RESPECTO A LOS CONFIGLETS (NO RESPECTO A LOS PARAMETROS)
                                     * CASO 1: EXISTE configlet en bbdd con mismo NOMBRE y mismo configletID -> no hacer nada y pasar a mirar los parametros
                                     * CASO 2: EXISTE configlet en bbdd con mismo NOMBRE y distinto configletID
                                     *  - ACTUALIZAR el configletID en la tabla VBX_EM_CONFIGLETS
                                     * CASO 3: EXISTE configlet en bbdd y NO EXISTE en el EMS -> Marcar para eliminar (con un -1 en el configletID)
                                     * CASO 4: NO EXISTE configlet en bbdd y SI EXISTE en el EMS
                                     *  -> Ver si tiene parametros
                                     *      -> Si tiene parametros
                                     *          1) INSERTAR los parametros (VBX_EM_CONFIGLETS_PARAMS)
                                     *          2) INSERTAR el nuevo configlet (VBX_EM_CONFIGLETS)
                                     *      -> Si NO tiene parametros
                                     *          1) INSERTAR el nuevo configlet (VBX_EM_CONFIGLETS)
                                     */
                                    /**
                                     * SITUACIONES QUE PUEDEN DARSE UNICAMENTE RESPECTO DE LOS PARAMETROS DE UN CONFIGLET específico (con el configletID que hemos descargado)
                                     * CASO 1: EXISTE el param_name en bbdd para el configletID
                                     *  -> Comparar el PARAM_ID de bbdd con el descargado
                                     *      -> ¿SON IGUALES?
                                     *      -> SI -> NO HACER NADA Y PASAR AL SIGUIENTE PARAMETRO SI ES QUE HAY MAS
                                     *      -> NO -> ACTUALIZAR EL PARAM_ID (VBX_EM_CONFIGLETS_PARAMETERS)
                                     * CASO 2: NO EXISTE el param_name en bbdd para el configletID -> INSERTAR EL NUEVO PARAMETRO
                                     * CASO 3: EXISTEN 'n' parametros en bbdd que NO existen en el EMS -> ACTUALIZAR el PARAM_ID = -1 y CONFIGLET_ID = -1 de ese parametro que ya no existe
                                     */
                                    log.info("#################################")
                                    //log.info("PROCESADOS " + totalConfigletsProcessed + " CONFIGLETS")
                                    //log.info("QUEDAN POR PROCESAR " + (totaldownloadedConfiglets - totalConfigletsProcessed) + " CONFIGLETS")
                                    log.info("PROCESSED " + totalConfigletsProcessed + " CONFIGLETS")
                                    log.info("THERE ARE " + (totaldownloadedConfiglets - totalConfigletsProcessed) + " REMAINING CONFIGLETS TO PROCESS")
                                    log.info("#################################")
                                    Integer downloadedConfigletID = (Integer) downloadedConfiglets.getAt(downloadedConfigletName)
                                    //log.info("Procesando configlet descargado con nombre " + downloadedConfigletName + " e ID = " + downloadedConfigletID)
                                    //log.info("Recuperando desde el EMS los parametros asociados al configlet con ID " + downloadedConfigletID)
                                    log.info("Processing downloaded configlet with name " + downloadedConfigletName + " and ID = " + downloadedConfigletID)
                                    log.info("Retrieving from the EMS the parameters associated with the configlet with ID " + downloadedConfigletID)
                                    def downloadedConfigletsParams = myEMSDAO.getAllParamsForConfiglet(String.valueOf(downloadedConfigletID))
                                    //log.info("Comprobando si existe el configlet descargado " + downloadedConfigletName + " en la base de datos local")
                                    log.info("Checking if the downloaded configlet with name " + downloadedConfigletName + " exists in the local database")
                                    def transformedLocal = transformedLocalConfigletsHashMap.getAt(downloadedConfigletName)
                                    Integer localConfigletID
                                    if (transformedLocal != null) {
                                        localConfigletID = Integer.valueOf(transformedLocal)
                                        //Cuidado que aquí puede que ya no exista
                                    }
                                    if (localConfigletID != null) {
                                        /**
                                         * CASOS 1,2
                                         */
                                        //log.info("Existe el configlet " + downloadedConfigletName + " en la base de datos local con ID LOCAL " + localConfigletID)
                                        //log.info("Comprobando si el ID del configlet local: " + localConfigletID + " es igual al ID del configlet descargado: " + downloadedConfigletID)
                                        log.info("The configlet with name " + downloadedConfigletName + " exists in the local database with LOCAL ID " + localConfigletID)
                                        log.info("Checking if the local configlet ID: " + localConfigletID + " is the same as the ID of the downloaded configlet: " + downloadedConfigletID)
                                        if (localConfigletID != downloadedConfigletID) {
                                            /**
                                             * CASO 2: EXISTE configlet en bbdd con mismo NOMBRE y distinto configletID
                                             *   - ACTUALIZAR el configletID en la tabla VBX_EM_CONFIGLETS
                                             */
                                            //log.info("Actualizando el configlet LOCAL con ID " + localConfigletID + " por su nuevo ID descargado: " + downloadedConfigletID + " en la tabla VBX_EM_CONFIGLETS")
                                            log.info("Updating the LOCAL configlet with ID " + localConfigletID + " with its new downloaded ID: " + downloadedConfigletID + " in the VBX_EM_CONFIGLETS database table")
                                            try {
                                                def resultUpdate = mySDDAO.updateConfigletID(String.valueOf(localConfigletID), String.valueOf(downloadedConfigletID))
                                                if (resultUpdate > 0) {
                                                    //log.info("Actualizado configletID " + localConfigletID + " por el nuevo " + downloadedConfigletID + " en la tabla VBX_EM_CONFIGLETS")
                                                    log.info("Updated configlet with ID " + localConfigletID + " with its new ID " + downloadedConfigletID + " in the VBX_EM_CONFIGLETS database table")
                                                    //localConfigletID = downloadedConfigletID //No lo modifico para luego poder actualizar la tabla VBX_EM_CONFIGLETS_PARAMS
                                                } else if (resultUpdate != 0) {
                                                    //log.error("Error al actualizar el configletID " + localConfigletID + " por el nuevo " + downloadedConfigletID + " en la tabla VBX_EM_CONFIGLETS")
                                                    log.error("Error updating configletID " + localConfigletID + " with its new ID " + downloadedConfigletID + " in the VBX_EM_CONFIGLETS database table")
                                                } else {
                                                    //log.info("No ha sido necesario actualizar el configlet con ID " + downloadedConfigletID + " al haber sido ya actualizado")
                                                    log.info("It was not necessary to update the configlet with ID " + downloadedConfigletID + " as it has already been updated")
                                                    //localConfigletID = downloadedConfigletID //No lo modifico para luego poder actualizar la tabla VBX_EM_CONFIGLETS_PARAMS
                                                }
                                            } catch (Exception e) {
                                                //log.fatal("ERROR AL ACTUALIZAR LOS IDS DE LOS CONFIGLETS EN LA BASE DE DATOS")
                                                log.fatal("ERROR WHEN UPDATING THE IDS OF THE CONFIGLETS IN THE DATABASE")
                                                log.fatal(e.printStackTrace())
                                                log.info("Checking if a configlets backup file exists")
                                                if(configletsBackupFile!=null){
                                                    //log.info("Existe archivo de backup de configlets")
                                                    log.info("There is a configlet backup file")
                                                    String pathAux = configletsBackupFile.getPath()
                                                    //log.fatal("Ejecutando archivo de backup de configlets " + pathAux)
                                                    log.fatal("Running configlets backup file " + pathAux)
                                                    ExecuteSQLFile executeSQLFile = new ExecuteSQLFile()
                                                    executeSQLFile.setIPDatabaseAddress(IPDatabaseAddress)
                                                    executeSQLFile.setIPDatabasePort(IPDatabasePort)
                                                    if(serviceName != null && !serviceName.isEmpty()){
                                                        executeSQLFile.setserviceName(serviceName)
                                                    }
                                                    executeSQLFile.registerSQLAndExecuteSQLFile(pathAux)
                                                    //log.info("Cerrando conexión a la base de datos")
                                                    log.info("Closing database connection")
                                                    mySDDAO.closeSDConnection()
                                                    log.info("EXITING. UpdateConfiglets")
                                                    System.exit(-1)
                                                }else{
                                                    //log.info("No existe archivo de backup de configlets al no haber previamente configlets en la base de datos")
                                                    //log.info("Cerrando conexión a la base de datos")
                                                    log.info("There is no configlet backup file because there are no previous configlets in the database")
                                                    log.info("Closing database connection")
                                                    mySDDAO.closeSDConnection()
                                                    log.info("EXITING. UpdateConfiglets")
                                                    System.exit(-1)
                                                }
                                            }
                                        } else {
                                            //log.info("Los IDs son iguales")
                                            //log.info("No se realiza ninguna actualización de IDs de configlets en la tabla VBX_EM_CONFIGLETS_PARAMS ni en la tabla VBX_EM_CONFIGLETS")
                                            log.info("IDs are equal")
                                            log.info("There is no update of configlet IDs in the VBX_EM_CONFIGLETS table")
                                        }
                                        //log.info("Recuperando desde local los parametros asociados con el configletID PREVIO " + localConfigletID + " equivalente al nuevo ID: " + downloadedConfigletID)
                                        log.info("Retrieving from the local database the parameters associated with the PREVIOUS configletID " + localConfigletID + " equivalent to the new ID: " + downloadedConfigletID)
                                        def localConfigletParams = mySDDAO.getLocalConfigletParamsByConfigletID(String.valueOf(localConfigletID))
                                        //log.info("Comprobando si existen parametros descargados desde el EMS para el configletID " + downloadedConfigletID + " equivalente al local ID: " + localConfigletID)
                                        log.info("Checking if there are parameters downloaded from the EMS for the configletID " + downloadedConfigletID + " equivalent to local ID: " + localConfigletID)
                                        if (downloadedConfigletsParams != null && downloadedConfigletsParams.size() > 0) {
                                            //Existen parametros en el EMS para el configlet
                                            //log.info("Existen parametros asociados al configlet " + downloadedConfigletID + " en el EMS")
                                            log.info("There are parameters associated with the configlet " + downloadedConfigletID + " in the EMS")
                                            if (localConfigletParams != null && localConfigletParams.size() > 0) {
                                                //Hay parametros en local
                                                //log.info("Existen parametros en local asociados al configlet " + downloadedConfigletID + " equivalente al local ID: " + localConfigletID)
                                                log.info("There are local parameters associated with the configlet " + downloadedConfigletID + " equivalent to the local ID: " + localConfigletID)
                                                //PASO 9 Comparar el ID del parametro que hay en base de datos con el descargado
                                                //log.info("Recorriendo los parametros existentes en la bbdd local asociados al configlet local ID: " + localConfigletID + " equivalente al nuevo ID: " + downloadedConfigletID)
                                                log.info("Looping the existing parameters in the local database associated with the local configlet ID: " + localConfigletID + " equivalent to the new ID: " + downloadedConfigletID)
                                                for (GroovyRowResult grr : localConfigletParams) {
                                                    String nameParamLocal = grr.getProperty("PARAM_NAME")
                                                    String idParamLocal = grr.getProperty("PARAM_ID")
                                                    //log.info("Comprobando la existencia del parametro LOCAL con nombre " + nameParamLocal + " en el configlet descargado")
                                                    log.info("Checking the existence of the LOCAL parameter with name " + nameParamLocal + " in the downloaded configlet parameters list")
                                                    String idParamDownloaded = downloadedConfigletsParams.getAt(nameParamLocal)
                                                    //Cuidado que puede que ya no exista
                                                    if (idParamDownloaded != null) {
                                                        //log.info("Existe el parametro con nombre " + nameParamLocal + " en la base de datos")
                                                        //log.info("Comparando el id del parametro local:" + idParamLocal + " con el id del parametro descargado: " + idParamDownloaded)
                                                        log.info("The parameter with name " + nameParamLocal + " exists in the local database")
                                                        log.info("Comparing the ID of the local parameter: " + idParamLocal + " with the ID of the downloaded parameter: " + idParamDownloaded)
                                                        if (idParamLocal != idParamDownloaded) {
                                                            /*log.info("Los IDs de los parámetros son distintos")
                                                            log.info("Actualizando el ID del parametro y el ID de configlet en la tabla VBX_EM_CONFIGLETS_PARAMS del parametro con nombre " + nameParamLocal)
                                                            log.info("Nuevo ID de parámetro: " + idParamDownloaded)
                                                            log.info("ID de configlet actualizado: " + downloadedConfigletID)*/
                                                            log.info("The IDs of the parameters are different")
                                                            log.info("Updating the parameter ID and configlet ID in the VBX_EM_CONFIGLETS_PARAMS table of the named parameter " + nameParamLocal)
                                                            log.info("New parameter ID: " + idParamDownloaded)
                                                            log.info("Updated configlet ID: " + downloadedConfigletID)
                                                            //VER SI YA DE PASO ACTUALIZO TANTO EL NUEVO CONFIGLET ID COMO EL DEL PARAMETRO
                                                            try{
                                                                def updateParamResult = mySDDAO.updateParamAndConfigID(nameParamLocal, idParamLocal, idParamDownloaded, String.valueOf(localConfigletID), String.valueOf(downloadedConfigletID))
                                                                if (updateParamResult > 0) {
                                                                    //log.info("Actualizado el ID del parametro con nombre " + nameParamLocal + " con el nuevo ID " + idParamDownloaded)
                                                                    //log.info("Actualizado el ID del configlet del parametro con ID " + idParamLocal + " con el ID de configlet descargado " + downloadedConfigletID)
                                                                    log.info("Updated the named parameter ID " + nameParamLocal + " with the new ID " + idParamDownloaded)
                                                                    log.info("Updated the parameter configlet ID with ID " + idParamLocal + " with the downloaded configlet ID " + downloadedConfigletID)
                                                                } else {
                                                                    //log.info("No se ha actualizado el ID del parametro con nombre " + nameParamLocal + " con el nuevo ID " + idParamDownloaded)
                                                                    log.info("The local parameter with name " + nameParamLocal + " has not been updated with the new ID " + idParamDownloaded)
                                                                }
                                                            }catch(Exception e){
                                                                ManageDatabaseException_IN manageDatabaseException_in1 = new ManageDatabaseException_IN();
                                                                manageDatabaseException_in1.setLocalConfigletID(localConfigletID)
                                                                manageDatabaseException_in1.setE(e)
                                                                manageDatabaseException_in1.setConfigletsParamsBackupFile(configletsParamsBackupFile)
                                                                manageDatabaseException_in1.setIPDatabaseAddress(IPDatabaseAddress)
                                                                manageDatabaseException_in1.setIPDatabasePort(IPDatabasePort)
                                                                manageDatabaseException_in1.setServiceName(serviceName)
                                                                manageDatabaseException_in1.setMySDDAO(mySDDAO)

                                                                manageDatabaseException_v2(manageDatabaseException_in1)

                                                                //manageDatabaseException(localConfigletID, e, configletsParamsBackupFile, IPDatabaseAddress, IPDatabasePort, serviceName, mySDDAO)
                                                            }
                                                        } else {
                                                            //log.info("Los IDs del parametro con nombre " + nameParamLocal + " son iguales")
                                                            //Los IDS de los parametros son iguales (Hay que actualizar el ID del configlet para este parametro
                                                            log.info("The IDs of the parameter with name " + nameParamLocal + " are equals.")
                                                            log.info("Updating the configlet ID of the paramID " + idParamLocal + " with its new value " + downloadedConfigletID)
                                                            def updateConfigletIDResult = mySDDAO.updateConfigletIDInParametersTable(localConfigletID,downloadedConfigletID)
                                                            if(updateConfigletIDResult > 0){
                                                                log.info("Updated the configlet ID of the parameter with ID " + idParamLocal + " with the downloaded configlet ID " + downloadedConfigletID)
                                                            }else{
                                                                log.error("The configlet ID of the parameter with ID " + idParamLocal + " has not been updated with the new configlet ID value " + downloadedConfigletID)
                                                            }
                                                        }
                                                    } else {
                                                        //Hay que marcar que este parametro LOCAL ya no forma parte del configlet //YA NO existe en el EMS
                                                        //UPDATE VBX_EM_CONFIGLETS_PARAMS set CONFIGLET_ID = -1 WHERE CONFILGLET_ID = ? AND PARAM_NAME = ?
                                                        //log.info("No existe el parametro LOCAL con nombre " + nameParamLocal + " en el configlet descargado")
                                                        //log.info("Marcando en bbdd con un -1 para eliminar el parametro LOCAL con nombre " + nameParamLocal + " e ID: " + idParamLocal)
                                                        log.info("The LOCAL parameter with name does not exist " + nameParamLocal + " in the downloaded configlet")
                                                        log.info("Updating its value to -1 to remove the named LOCAL parameter " + nameParamLocal + " and local ID: " + idParamLocal)
                                                        try{
                                                            def resultMarkToDeleteParameter = mySDDAO.markToDeleteConfigletParamDueToNonExistingConfigletParam(idParamLocal)
                                                            if (resultMarkToDeleteParameter > 0) {
                                                                //log.info("Se ha marcado para eliminar el parametro " + nameParamLocal + " de la bbdd local")
                                                                log.info("Changed to -1 the " + nameParamLocal + " parameter to delete it from the local database")
                                                            } else {
                                                                log.error("Error when marking to delete the LOCAL parameter " + nameParamLocal + " in the local database")
                                                            }
                                                        }catch(Exception e){
                                                            ManageDatabaseException_IN manageDatabaseException_in1 = new ManageDatabaseException_IN();
                                                            manageDatabaseException_in1.setLocalConfigletID(localConfigletID)
                                                            manageDatabaseException_in1.setE(e)
                                                            manageDatabaseException_in1.setConfigletsParamsBackupFile(configletsParamsBackupFile)
                                                            manageDatabaseException_in1.setIPDatabaseAddress(IPDatabaseAddress)
                                                            manageDatabaseException_in1.setIPDatabasePort(IPDatabasePort)
                                                            manageDatabaseException_in1.setServiceName(serviceName)
                                                            manageDatabaseException_in1.setMySDDAO(mySDDAO)

                                                            manageDatabaseException_v2(manageDatabaseException_in1)

                                                            //manageDatabaseException(localConfigletID, e, configletsParamsBackupFile, IPDatabaseAddress, IPDatabasePort, serviceName, mySDDAO)
                                                        }
                                                        log.info("-----")
                                                    }
                                                    //Comprobar si existen nuevos parametros para el configletDescargado que no existen en la bbdd
                                                    //log.info("Comprobando la existencia de nuevos parametros en el EMS que no existan en la base de datos")
                                                    log.info("Checking the existence of new parameters in the EMS that do not exist in the database")
                                                    Set<String> downloadedConfigletParamNamesKeys = downloadedConfigletsParams.keySet()
                                                    HashMap newConfigletParameters = new HashMap()
                                                    //K = Param_name V = param_id
                                                    for (String configletParamName : downloadedConfigletParamNamesKeys) {
                                                        boolean existsInDatabase = false
                                                        for (GroovyRowResult grr2 : localConfigletParams) {
                                                            nameParamLocal = grr2.getProperty("PARAM_NAME")
                                                            if (configletParamName == nameParamLocal) {
                                                                existsInDatabase = true
                                                                break
                                                            }
                                                        }
                                                        if (!existsInDatabase) {
                                                            //log.info("No existe en bbdd el parametro " + configletParamName + " del configlet " + downloadedConfigletID)
                                                            //log.info("Añadiendo el " + configletParamName + " al HashMap para insertarlo como nuevo parametro en la tabla VBX_EM_CONFIGLETS_PARAMS")
                                                            log.info("The parameter " + configletParamName + "of the " + downloadedConfigletID + " configlet doesn't exist in the database")
                                                            log.info("Adding the " + configletParamName + " configlet to the HashMap to insert it as a new parameter in the VBX_EM_CONFIGLETS_PARAMS table")
                                                            newConfigletParameters.put(configletParamName, downloadedConfigletsParams.get(configletParamName))
                                                        }
                                                    }
                                                    //log.info("Comprobando si hay pendientes nuevos parametros para insertarlos en la tabla VBX_EM_CONFIGLETS_PARAMS")
                                                    log.info("Checking if there are pending new parameters to be inserted in the VBX_EM_CONFIGLETS_PARAMS table")
                                                    if (!newConfigletParameters.isEmpty()) {
                                                        //Existen nuevos parametros en el EMS que no existen en Local
                                                        //K = name V = param_id
                                                        //log.info("Existen nuevos parametros asociados al configletID " + downloadedConfigletID + " que deben de insertarse en la tabla VBX_EM_CONFIGLETS_PARAMS")
                                                        log.info("There are new parameters associated with configletID " + downloadedConfigletID + " to be inserted in the VBX_EM_CONFIGLETS_PARAMS table")
                                                        Set<String> newConfigletsParamsNames = newConfigletParameters.keySet()
                                                        for (String sAux : newConfigletsParamsNames) {
                                                            //Insert String paramName, String paramID, String configletID
                                                            String paramID = newConfigletParameters.get(sAux)
                                                            try{
                                                                def result = mySDDAO.insertNewConfigletParam(sAux, paramID, downloadedConfigletID)
                                                                if (result != null) {
                                                                    //log.info("Se ha insertado el nuevo parametro " + sAux + " con ID " + paramID + " del configlet " + downloadedConfigletID)
                                                                    log.info("The new parameter " + sAux + " has been inserted with ID " + paramID + " of the " + downloadedConfigletID + " configlet")
                                                                } else {
                                                                    //log.error("Error al insertar el nuevo parametro " + sAux + " con ID " + paramID + " del configlet " + downloadedConfigletID)
                                                                    log.error("Error inserting new parameter " + sAux + " with ID " + paramID + " of the " + downloadedConfigletID + " configlet")
                                                                }
                                                            }catch(Exception e){
                                                                ManageDatabaseException_IN manageDatabaseException_in1 = new ManageDatabaseException_IN();
                                                                manageDatabaseException_in1.setLocalConfigletID(localConfigletID)
                                                                manageDatabaseException_in1.setE(e)
                                                                manageDatabaseException_in1.setConfigletsParamsBackupFile(configletsParamsBackupFile)
                                                                manageDatabaseException_in1.setIPDatabaseAddress(IPDatabaseAddress)
                                                                manageDatabaseException_in1.setIPDatabasePort(IPDatabasePort)
                                                                manageDatabaseException_in1.setServiceName(serviceName)
                                                                manageDatabaseException_in1.setMySDDAO(mySDDAO)

                                                                manageDatabaseException_v2(manageDatabaseException_in1)

                                                                //manageDatabaseException(localConfigletID, e, configletsParamsBackupFile, IPDatabaseAddress, IPDatabasePort, serviceName, mySDDAO)
                                                            }
                                                        }
                                                    } else {
                                                        //log.info("No existen nuevos parametros asociados al configletID " + downloadedConfigletID + " que deban de insertarse en la bbdd local")
                                                        log.info("There are no new parameters associated with the configletID " + downloadedConfigletID + " that should be inserted in the local database")
                                                    }
                                                    //log.info("Eliminando (si existen) parametros marcados en la tabla VBX_EM_CONFIGLETS_PARAMS a eliminar")
                                                    log.info("Deleting (if any) the parameters marked in the VBX_EM_CONFIGLETS_PARAMS table to be deleted")
                                                    try{
                                                        def resultDeleteMarkedParameters = mySDDAO.deleteMarkedConfigletsParams()
                                                        if (resultDeleteMarkedParameters) {
                                                            //log.info("Se han eliminado los parametros marcados para eliminar")
                                                            log.info("Parameters marked to be deleted have been removed from the LOCAL database")
                                                        } else {
                                                            //log.info("No se ha eliminado ningún parámetro")
                                                            log.info("No parameters have been deleted")
                                                        }
                                                    }catch(Exception e){
                                                        //log.fatal("ERROR AL ELIMINAR TODOS LOS PARAMETROS MARCADOS PARA ELIMINAR DE LA TABLA VBX_EM_CONFIGLETS_PARAMS")
                                                        log.fatal("ERROR WHEN DELETING ALL PARAMETERS MARKED TO BE DELETED FROM VBX_EM_CONFIGLETS_PARAMS TABLE")
                                                        ManageDatabaseException_IN manageDatabaseException_in1 = new ManageDatabaseException_IN();
                                                        manageDatabaseException_in1.setLocalConfigletID(localConfigletID)
                                                        manageDatabaseException_in1.setE(e)
                                                        manageDatabaseException_in1.setConfigletsParamsBackupFile(configletsParamsBackupFile)
                                                        manageDatabaseException_in1.setIPDatabaseAddress(IPDatabaseAddress)
                                                        manageDatabaseException_in1.setIPDatabasePort(IPDatabasePort)
                                                        manageDatabaseException_in1.setServiceName(serviceName)
                                                        manageDatabaseException_in1.setMySDDAO(mySDDAO)

                                                        manageDatabaseException_v2(manageDatabaseException_in1)

                                                        //manageDatabaseException(localConfigletID, e, configletsParamsBackupFile, IPDatabaseAddress, IPDatabasePort, serviceName, mySDDAO)
                                                    }
                                                }
                                            }
                                            else {
                                                //No existen parametros en LOCAL asociados al configlet en el EMS Y SI QUE EXISTEN EN EL EMS
                                                //HABRÁ QUE INSERTAR LOS NUEVOS PARAMETROS EN LA BBDD
                                                Set<String> downloadedConfigletParamNamesKeys = downloadedConfigletsParams.keySet()
                                                HashMap newConfigletParameters = new HashMap()
                                                //K = Param_name V = param_id
                                                for (String configletParamName : downloadedConfigletParamNamesKeys) {
                                                    boolean existsInDatabase = false
                                                    for (GroovyRowResult grr : localConfigletParams) {
                                                        String nameParamLocal = grr.getProperty("PARAM_NAME")
                                                        if (configletParamName == nameParamLocal) {
                                                            existsInDatabase = true
                                                            break
                                                        }
                                                    }
                                                    if (!existsInDatabase) {
                                                        //log.info("No existe en bbdd el parametro " + configletParamName + " del configlet " + downloadedConfigletID)
                                                        //log.info("Añadiendo el " + configletParamName + " para insertarlo como nuevo parametro en la tabla VBX_EM_CONFIGLETS_PARAMS")
                                                        log.info("The parameter " + configletParamName + " of the configlet " + downloadedConfigletID + " does not exist in the database")
                                                        log.info("Adding the " + configletParamName + " parameter to the HashMap to insert it into VBX_EM_CONFIGLETS_PARAMS database table")
                                                        newConfigletParameters.put(configletParamName, downloadedConfigletsParams.get(configletParamName))
                                                    }
                                                }
                                                if (!newConfigletParameters.isEmpty()) {
                                                    //Existen nuevos parametros en el EMS que no existen en Local
                                                    //K = name V = param_id
                                                    //log.info("Existen parametros en el EMS y asociados al configlet " + downloadedConfigletID + " NO existentes en la base de datos")
                                                    log.info("There are parameters in the EMS associated with the configlet " + downloadedConfigletID + " which don't exist in the local database")
                                                    Set<String> newConfigletsParamsNames = newConfigletParameters.keySet()
                                                    for (String sAux : newConfigletsParamsNames) {
                                                        //Insert String paramName, String paramID, String configletID
                                                        String paramID = newConfigletParameters.get(sAux)
                                                        //log.info("Insertando el nuevo parametro " + sAux + " con ID " + paramID + " del configlet " + downloadedConfigletID)
                                                        log.info("Inserting the new parameter " + sAux + " with ID " + paramID + " of the configlet " + downloadedConfigletID)
                                                        try{
                                                            def result = mySDDAO.insertNewConfigletParam(sAux, paramID, downloadedConfigletID)
                                                            if (result != null) {
                                                                //log.info("Se ha insertado el nuevo parametro " + sAux + " con ID " + paramID + " del configlet " + downloadedConfigletID)
                                                                log.info("The new parameter " + sAux + " with ID " + paramID + " of the configlet " + downloadedConfigletID + " has been inserted")
                                                            } else {
                                                                //log.error("Error al insertar el nuevo parametro " + sAux + " con ID " + paramID + " del configlet " + downloadedConfigletID)
                                                                log.error("Error inserting the new parameter " + sAux + " with ID " + paramID + " of the configlet " + downloadedConfigletID)
                                                            }
                                                        }catch(Exception e){
                                                            //log.fatal("ERROR AL INSERTAR EL NUEVO PARAMETRO " + sAux)
                                                            log.fatal("ERROR INSERTING THE NEW PARAMETER " + sAux)
                                                            ManageDatabaseException_IN manageDatabaseException_in1 = new ManageDatabaseException_IN();
                                                            manageDatabaseException_in1.setLocalConfigletID(localConfigletID)
                                                            manageDatabaseException_in1.setE(e)
                                                            manageDatabaseException_in1.setConfigletsParamsBackupFile(configletsParamsBackupFile)
                                                            manageDatabaseException_in1.setIPDatabaseAddress(IPDatabaseAddress)
                                                            manageDatabaseException_in1.setIPDatabasePort(IPDatabasePort)
                                                            manageDatabaseException_in1.setServiceName(serviceName)
                                                            manageDatabaseException_in1.setMySDDAO(mySDDAO)

                                                            manageDatabaseException_v2(manageDatabaseException_in1)

                                                            //manageDatabaseException(localConfigletID, e, configletsParamsBackupFile, IPDatabaseAddress, IPDatabasePort, serviceName, mySDDAO)
                                                        }
                                                    }
                                                } else {
                                                    //log.info("No existen nuevos parametros asociados al configletID " + downloadedConfigletID + " que deban de insertarse en la bbdd")
                                                    log.info("There are no new parameters associated with the configletID " + downloadedConfigletID + " wich must be inserted in the local database")
                                                }
                                            }
                                        } else { //NO EXISTEN PARAMETROS PARA EL CONFIGLET EN EL EMS
                                            //log.info("No existen parametros asociados al configlet " + downloadedConfigletID + " en el EMS")
                                            log.info("There are no parameters associated with the downloaded configlet with ID " + downloadedConfigletID + " matching with the local configlet ID " + localConfigletID + " in the EMS")
                                            //¿EXISTEN PARAMETROS EN LOCAL ASOCIADOS AL CONFIGLET QUE HAY QUE MARCAR PARA ELIMINAR?
                                            //log.info("Comprobando si existen parametros en local asociados al configlet " + downloadedConfigletID)
                                            log.info("Checking if there are local parameters associated with the downloaded configlet with ID " + downloadedConfigletID + " matching with the local configlet ID " + localConfigletID)
                                            if (localConfigletParams != null && localConfigletParams.size() > 0) {
                                                //CASO EN EL QUE EXISTAN PARA
                                                try{
                                                    if (mySDDAO.markToDeleteConfigletParamsForNonExistingConfigletID(localConfigletID) > 0) {
                                                        //log.info("Marcados para eliminar los parametros del configlet con id " + localConfigletID + " de la tabla VBX_EM_CONFIGLETS_PARAMS por no existir el configlet " + localConfigletID + " en el EMS")
                                                        log.info("Marked to remove the parameters of the configlet with LOCAL ID " + localConfigletID + " of the VBX_EM_CONFIGLETS_PARAMS database table because the configlet does not exist " + localConfigletID + " in the EMS")
                                                    } else {
                                                        //log.error("Error al intentar marcar para eliminar el configlet con id " + localConfigletID + " de la tabla VBX_EM_CONFIGLETS_PARAMS")
                                                        log.error("Error when trying to marking to remove the parameters of the configlet with LOCAL ID " + localConfigletID + " in the VBX_EM_CONFIGLETS_PARAMS database table")
                                                    }
                                                }catch(Exception e){
                                                    ManageDatabaseException_IN manageDatabaseException_in1 = new ManageDatabaseException_IN();
                                                    manageDatabaseException_in1.setLocalConfigletID(localConfigletID)
                                                    manageDatabaseException_in1.setE(e)
                                                    manageDatabaseException_in1.setConfigletsParamsBackupFile(configletsParamsBackupFile)
                                                    manageDatabaseException_in1.setIPDatabaseAddress(IPDatabaseAddress)
                                                    manageDatabaseException_in1.setIPDatabasePort(IPDatabasePort)
                                                    manageDatabaseException_in1.setServiceName(serviceName)
                                                    manageDatabaseException_in1.setMySDDAO(mySDDAO)

                                                    manageDatabaseException_v2(manageDatabaseException_in1)

                                                    //manageDatabaseException(localConfigletID, e, configletsParamsBackupFile, IPDatabaseAddress, IPDatabasePort, serviceName, mySDDAO)
                                                }
                                            } else {
                                                //log.info("No existen parametros en local asociados al configlet " + downloadedConfigletID)
                                                log.info("There are no local parameters associated with the configlet " + downloadedConfigletID)
                                            }
                                        }
                                    } else {
                                        //log.info("No existe el configlet con nombre = " + downloadedConfigletName + " en la base de datos local")
                                        //log.info("Insertando nuevo configlet en la bbdd local")
                                        log.info("There is no configlet with name " + downloadedConfigletName + " in the local database")
                                        log.info("Inserting new configlet in the local database")
                                        /**CASO 4: NO EXISTE configlet en bbdd y SI EXISTE en el EMS
                                         * -> 1) INSERTAR el configlet en la VBX_EM_CONFIGLETS
                                         *  -> 2)Ver si tiene parametro
                                         *   ->  - Si tiene parametros
                                         *       - INSERTAR los parametros (VBX_EM_CONFIGLETS_PARAMS)
                                         */
                                        //Insertar configlet
                                        try{
                                            def resultNewInsert = mySDDAO.insertNewConfiglet(downloadedConfigletName, String.valueOf(downloadedConfigletID))
                                            if (resultNewInsert != null && resultNewInsert.size() > 0) {
                                                //Ver si tiene parametros
                                                //log.info("Comprobando si tiene parametros")
                                                log.info("Checking if it has parameters")
                                                if (downloadedConfigletsParams != null && downloadedConfigletsParams.size() > 0) {
                                                    //TIENE PARAMETROS QUE HAY QUE INSERTAR
                                                    //log.info("El nuevo configlet " + downloadedConfigletID + " contiene parametros a insertar")
                                                    //log.info("Existen parametros en el EMS y asociados al configlet " + downloadedConfigletID + " NO existentes en la base de datos")
                                                    log.info("The new configlet with ID " + downloadedConfigletID + " contains parameters to insert")
                                                    log.info("There are parameters in the EMS associated with the configlet " + downloadedConfigletID + "which don't exist in the database")
                                                    Set<String> newConfigletsParamsNames = downloadedConfigletsParams.keySet()
                                                    for (String sAux : newConfigletsParamsNames) {
                                                        String paramID = downloadedConfigletsParams.get(sAux)
                                                        //log.info("Insertando el nuevo parametro " + sAux + " con ID " + paramID + " del configlet " + downloadedConfigletID)
                                                        log.info("Inserting the new parameter " + sAux + " with ID " + paramID + " of the configlet with ID " + downloadedConfigletID)
                                                        def result = mySDDAO.insertNewConfigletParam(sAux, paramID, downloadedConfigletID)
                                                        if (result != null && result.size() > 0) {
                                                            //log.info("Se ha insertado el nuevo parametro " + sAux + " con ID " + paramID + " del configlet " + downloadedConfigletID)
                                                            log.info("The new parameter has been inserted " + sAux + " with ID " + paramID + " of the configlet " + downloadedConfigletID)
                                                        } else {
                                                            //log.error("Error al insertar el nuevo parametro " + sAux + " con ID " + paramID + " del configlet " + downloadedConfigletID)
                                                            log.error("Error inserting new parameter " + sAux + " with ID " + paramID + " of the configlet " + downloadedConfigletID)
                                                        }
                                                    }
                                                } else {
                                                    //log.info("El nuevo configlet " + downloadedConfigletID + " no contiene parametros")
                                                    log.info("The new configlet with ID " + downloadedConfigletID + " doesn't contain parameters")
                                                }
                                            } else {
                                                //log.error("No se ha podido insertar el nuevo configlet con nombre " + downloadedConfigletName + " e ID " + downloadedConfigletID + " en la tabla VBX_EM_CONFIGLETS")
                                                log.error("Could not insert new named configlet " + downloadedConfigletName + " and ID " + downloadedConfigletID + " in the VBX_EM_CONFIGLETS database table")
                                            }
                                        }catch(Exception e){
                                            //log.fatal("ERROR AL INSERTAR EL NUEVO CONFIGLET " + downloadedConfigletID)
                                            log.fatal("ERROR WHEN INSERTING THE NEW CONFIGLET WITH ID " + downloadedConfigletID)
                                            log.fatal(e.printStackTrace())
                                            if(configletsBackupFile!=null){
                                                String pathAux = configletsBackupFile.getPath()
                                                //log.fatal("Ejecutando archivo de backup de los configlets " + pathAux)
                                                log.fatal("Executing backup file of configlets " + pathAux)
                                                ExecuteSQLFile executeSQLFile = new ExecuteSQLFile()
                                                executeSQLFile.setIPDatabaseAddress(IPDatabaseAddress)
                                                executeSQLFile.setIPDatabasePort(IPDatabasePort)
                                                if(serviceName != null && !serviceName.isEmpty()){
                                                    executeSQLFile.setserviceName(serviceName)
                                                }
                                                executeSQLFile.registerSQLAndExecuteSQLFile(pathAux)
                                                //log.info("Cerrando conexión a la base de datos")
                                                log.info("Closing database connection")
                                                mySDDAO.closeSDConnection()
                                                log.info("EXITING. UpdateConfiglets")
                                                System.exit(-1)
                                            }else{
                                                //log.info("No existe archivo de backup de configlets al no haber previamente configlets en la base de datos")
                                                //log.info("Cerrando conexión a la base de datos")
                                                log.info("There is no configlet backup file because there are no previous configlets in the database")
                                                log.info("Closing database connection")
                                                mySDDAO.closeSDConnection()
                                                log.info("EXITING. UpdateConfiglets")
                                                System.exit(-1)
                                            }
                                        }
                                    }
                                    totalConfigletsProcessed = totalConfigletsProcessed + 1
                                }//Fin del FOR
                                //log.info("FIN DEL RECORRIDO DE LOS CONFIGLETS DESCARGADOS")
                                //log.info("Comprobando si existen configlets en local NO existentes en el EMS")
                                log.info("END OF THE DOWNLOADED CONFIGLETS LOOP")
                                log.info("Checking if there are local configlets which don't exist in the EMS")
                                //CASO 3: EXISTE configlet en bbdd y NO EXISTE en el EMS -> Marcar para eliminar (con un -1 en el configletID)
                                //Y marcar tambien a eliminar los parametros en local que tuviera asociados
                                //if(!localConfiglets.contains(downloadedConfiglet)) //pero por nombre
                                for (GroovyRowResult grr : localConfiglets) {
                                    String configletName = grr.getProperty("CONFIGLET_NAME")
                                    String configletID = grr.getProperty("CONFIGLET_ID")
                                    if (configletName != null) {
                                        if (!downloadedConfiglets.containsKey(configletName)) {
                                            //Caso en el que hay un configlet en bbdd que NO existe en el EMS
                                            //log.info("El configlet " + configletName + " en la bbdd local NO existe en el EMS. Marcando para eliminar")
                                            log.info("The configlet with name " + configletName + " doesn't exist in the EMS. Putting a mark to delete the configlet")
                                            try{
                                                def updateToDeleteConfiglet = mySDDAO.markToDeleteLocalConfiglet(configletName)
                                                if (updateToDeleteConfiglet > 0) {
                                                    //log.info("Marcado para eliminar el configlet " + configletName + " de la tabla VBX_EM_CONFIGLETS")
                                                    log.info("Marked to remove the configlet " + configletName + " of the VBX_EM_CONFIGLETS database table")
                                                } else {
                                                    //log.error("No se ha podido marcar el configlet " + configletName + " para eliminar de la tabla VBX_EM_CONFIGLETS")
                                                    log.error("Could not put a mark on the configlet with name " + configletName + " to remove it from the VBX_EM_CONFIGLETS table")
                                                }
                                            }catch(Exception e){
                                                //log.fatal("ERROR AL MARCAR PARA ELIMINAR EL CONFIGLET " + configletName)
                                                log.fatal("ERROR WHEN PUTTING A MARK TO DELETE THE CONFIGLET WITH NAME " + configletName)
                                                ManageDatabaseException_IN manageDatabaseException_in1 = new ManageDatabaseException_IN();
                                                manageDatabaseException_in1.setLocalConfigletID(configletID)
                                                manageDatabaseException_in1.setE(e)
                                                manageDatabaseException_in1.setConfigletsParamsBackupFile(configletsParamsBackupFile)
                                                manageDatabaseException_in1.setIPDatabaseAddress(IPDatabaseAddress)
                                                manageDatabaseException_in1.setIPDatabasePort(IPDatabasePort)
                                                manageDatabaseException_in1.setServiceName(serviceName)
                                                manageDatabaseException_in1.setMySDDAO(mySDDAO)

                                                manageDatabaseException_v2(manageDatabaseException_in1)

                                                //manageDatabaseException(configletID, e, configletsParamsBackupFile, IPDatabaseAddress, IPDatabasePort, serviceName, mySDDAO)
                                            }
                                            try{
                                                def updateToDeleteConfigletInParametersTable = mySDDAO.markToDeleteConfigletParamsForNonExistingConfigletID(Integer.valueOf(configletID))
                                                if (updateToDeleteConfigletInParametersTable > 0) {
                                                    //log.info("Marcados para eliminar " + updateToDeleteConfigletInParametersTable + " parametros de la tabla VBX_EM_CONFIGLETS_PARAMS")
                                                    log.info("Put a mark to delete " + updateToDeleteConfigletInParametersTable + " parameters in the VBX_EM_CONFIGLETS_PARAMS table")
                                                } else {
                                                    //log.info("No se ha marcado ningun parametro a eliminar en la tabla VBX_EM_CONFIGLETS_PARAMS")
                                                    log.info("No parameters have been marked to be deleted in the VBX_EM_CONFIGLETS_PARAMS table")
                                                }
                                            }catch(Exception e){
                                                //log.fatal("ERROR AL MARCAR PARA ELIMINAR LOS PARAMETROS DEL CONFIGLET " + configletName)
                                                log.fatal("ERROR WHEN PUTTING A MARK TO DELETE ALL THE PARAMETERS OF THE CONFIGLET WITH NAME " + configletName)
                                                log.fatal(e.printStackTrace())
                                                if(configletsParamsBackupFile!=null){
                                                    String pathAux = configletsParamsBackupFile.getPath()
                                                    //log.fatal("Ejecutando archivo de backup de los parametros de los configlets " + pathAux)
                                                    log.fatal("Running configlet parameters backup file " + pathAux)
                                                    ExecuteSQLFile executeSQLFile = new ExecuteSQLFile()
                                                    executeSQLFile.setIPDatabaseAddress(IPDatabaseAddress)
                                                    executeSQLFile.setIPDatabasePort(IPDatabasePort)
                                                    if(serviceName != null && !serviceName.isEmpty()){
                                                        executeSQLFile.setserviceName(serviceName)
                                                    }
                                                    executeSQLFile.registerSQLAndExecuteSQLFile(pathAux)
                                                    //log.info("Cerrando conexión a la base de datos")
                                                    log.info("Closing database connection")
                                                    mySDDAO.closeSDConnection()
                                                    log.info("EXITING. UpdateConfiglets")
                                                    System.exit(-1)
                                                }else{
                                                    //log.info("No existe archivo de backup de parametros de configlets al no haber previamente parametros en la base de datos")
                                                    //log.info("Cerrando conexión a la base de datos")
                                                    log.info("There is no backup file of configlet parameters because there are no previously parameters in the database")
                                                    log.info("Closing database connection")
                                                    mySDDAO.closeSDConnection()
                                                    log.info("EXITING. UpdateConfiglets")
                                                    System.exit(-1)
                                                }
                                            }
                                        }
                                    }
                                }
                                //ELIMINAR los CONFIGLETS Y PARÁMETROS QUE ESTUVIERAN MARCADOS PARA ELIMINAR
                                //log.info("Eliminando los parametros de la tabla VBX_EM_CONFIGLETS_PARAMS marcados para eliminar")
                                log.info("Deleting the parameters from the VBX_EM_CONFIGLETS_PARAMS table marked to be removed")
                                def deletedParams = mySDDAO.deleteMarkedConfigletsParams()
                                if (deletedParams) {
                                    //log.info("Se han eliminado " + deletedParams + " parametros de la tabla VBX_EM_CONFIGLETS_PARAMS")
                                    log.info(deletedParams + " parameters have been removed from the VBX_EM_CONFIGLETS_PARAMS database table")
                                } else {
                                    //log.info("No se ha eliminado ningún parametro de la tabla VBX_EM_CONFIGLETS_PARAMS")
                                    log.info("No parameters have been removed from the VBX_EM_CONFIGLETS_PARAMS database table")
                                }
                                log.info("Deleting the configlets from the VBX_EM_CONFIGLETS table marked to be deleted")
                                def deletedConfiglets = mySDDAO.deleteMarkedConfiglets()
                                if (deletedConfiglets) {
                                    //log.info("Se han eliminado " + deletedConfiglets + " configlets de la tabla VBX_EM_CONFIGLETS")
                                    log.info(deletedConfiglets + " configlets have been deleted from the VBX_EM_CONFIGLETS database table")
                                } else {
                                    //log.info("No se ha eliminado ningún configlet de la tabla VBX_EM_CONFIGLETS")
                                    log.info("No configlet has been removed from the VBX_EM_CONFIGLETS table")
                                }
                            } else {
                                //log.info("No existen configlets en el EMS pertenecientes a la solución vbox")
                                log.info("There are no configlets in the EMS belonging to the vbox solution")
                            }
                        } else {
                            //log.info("No existen configlets en la base de datos")
                            //log.info("Comprobando conexión al EMS en el puerto " + EMSIPPort)
                            log.info("There are no configlets in the database")
                            log.info("Checking connection to the EMS in the port " + EMSIPPort)
                            if (!myEMSDAO.checkEMSConnectivity()) {
                                //log.error("No se ha podido establecer una conexión con el EMS en el puerto " + EMSIPPort)
                                //log.info("Cerrando conexión a la base de datos")
                                log.error("Unable to establish a connection with the EMS on the port " + EMSIPPort)
                                log.info("Closing database connection")
                                mySDDAO.closeSDConnection()
                                log.info("EXITING. UpdateConfiglets")
                                System.exit(-1)
                            }
                            //log.info("Descargando configlets")
                            log.info("Downloading configlets")
                            def downloadedConfiglets = myEMSDAO.getAllConfiglets() //Esto es un GroovyRowResult
                            if (downloadedConfiglets != null && downloadedConfiglets.size() > 0) {
                                def totaldownloadedConfiglets = downloadedConfiglets.size()
                                //log.info("Se han recuperado " + totaldownloadedConfiglets + " configlets desde el EMS pertenecientes a la solución vbox")
                                log.info(totaldownloadedConfiglets + " configlets retrieved from the EMS belonging to the vbox solution")
                                //PASO 3: COMPARAR EL ID ALMACENADO CON EL DESCARGADO
                                Set keys = downloadedConfiglets.keySet()
                                //log.info("Recorriendo los configlets descargados")
                                log.info("Looping downloaded configlets")
                                for (String downloadedConfigletName : keys) {
                                    log.info("#################################")
                                    //log.info("PROCESADOS " + totalConfigletsProcessed + " CONFIGLETS")
                                    //log.info("QUEDAN POR PROCESAR " + (totaldownloadedConfiglets - totalConfigletsProcessed) + " CONFIGLETS")
                                    log.info("PROCESSED " + totalConfigletsProcessed + " CONFIGLETS")
                                    log.info("THERE ARE " + (totaldownloadedConfiglets - totalConfigletsProcessed) + " NON PROCESSED CONFIGLETS")
                                    log.info("#################################")
                                    Integer downloadedConfigletID = (Integer) downloadedConfiglets.getAt(downloadedConfigletName)
                                    //log.info("Procesando configlet descargado con nombre " + downloadedConfigletName + " e ID = " + downloadedConfigletID)
                                    //log.info("Recuperando desde el EMS los parametros asociados al configlet con ID " + downloadedConfigletID)
                                    log.info("Processing downloaded configlet with name " + downloadedConfigletName + " and ID = " + downloadedConfigletID)
                                    log.info("Retrieving all the parameters from the EMS associated with the configlet ID " + downloadedConfigletID)
                                    def downloadedConfigletsParams = myEMSDAO.getAllParamsForConfiglet(String.valueOf(downloadedConfigletID))
                                    //log.info("Insertando nuevo configlet en la bbbdd local")
                                    log.info("Inserting new configlet in the local database")
                                    try{
                                        def resultNewInsert = mySDDAO.insertNewConfiglet(downloadedConfigletName, String.valueOf(downloadedConfigletID))
                                        if (resultNewInsert != null && resultNewInsert.size() > 0) {
                                            //Ver si tiene parametros
                                            //log.info("Comprobando si tiene parametros")
                                            log.info("Checking if the configlet has parameters ")
                                            if (downloadedConfigletsParams != null && downloadedConfigletsParams.size() > 0) {
                                                //TIENE PARAMETROS QUE HAY QUE INSERTAR
                                                //log.info("El nuevo configlet " + downloadedConfigletID + " contiene parametros a insertar")
                                                //log.info("Existen parametros en el EMS y asociados al configlet " + downloadedConfigletID + " NO existentes en la base de datos")
                                                log.info("The new configlet with ID " + downloadedConfigletID + " contains new parameters which must be inserted")
                                                log.info("There are parameters in the EMS associated with the configlet " + downloadedConfigletID + " which don't exist in the database")
                                                Set<String> newConfigletsParamsNames = downloadedConfigletsParams.keySet()
                                                for (String sAux : newConfigletsParamsNames) {
                                                    String paramID = downloadedConfigletsParams.get(sAux)
                                                    //log.info("Insertando el nuevo parametro " + sAux + " con ID " + paramID + " del configlet " + downloadedConfigletID)
                                                    log.info("Inserting new parameter " + sAux + " with ID " + paramID + " of the configlet with ID " + downloadedConfigletID)
                                                    def result = mySDDAO.insertNewConfigletParam(sAux, paramID, downloadedConfigletID)
                                                    if (result != null && result.size() > 0) {
                                                        //log.info("Se ha insertado el nuevo parametro " + sAux + " con ID " + paramID + " del configlet " + downloadedConfigletID)
                                                        log.info("The new parameter " + sAux + " with ID " + paramID + " of the configlet with ID " + downloadedConfigletID + " has been inserted")
                                                    } else {
                                                        //log.error("Error al insertar el nuevo parametro " + sAux + " con ID " + paramID + " del configlet " + downloadedConfigletID)
                                                        log.error("Error inserting new parameter " + sAux + " with ID " + paramID + " of the configlet " + downloadedConfigletID)
                                                    }
                                                }
                                            } else {
                                                //log.info("El nuevo configlet " + downloadedConfigletID + " no contiene parametros")
                                                log.info("The new configlet with ID " + downloadedConfigletID + " doesn't contain parameters")
                                            }
                                        } else {
                                            //log.error("No se ha podido insertar el nuevo configlet con nombre " + downloadedConfigletName + " e ID " + downloadedConfigletID + " en la tabla VBX_EM_CONFIGLETS")
                                            log.error("Could not insert the new configlet with name " + downloadedConfigletName + " and ID " + downloadedConfigletID + " in the VBX_EM_CONFIGLETS table")
                                        }
                                    }catch(Exception e){
                                        //log.fatal("ERROR AL INSERTAR EL NUEVO CONFIGLET " + downloadedConfigletID)
                                        log.fatal("ERROR WHEN INSERTING THE NEW CONFIGLET WITH ID " + downloadedConfigletID)
                                        ManageDatabaseException_IN manageDatabaseException_in1 = new ManageDatabaseException_IN();
                                        manageDatabaseException_in1.setLocalConfigletID(downloadedConfigletID)
                                        manageDatabaseException_in1.setE(e)
                                        manageDatabaseException_in1.setConfigletsParamsBackupFile(configletsParamsBackupFile)
                                        manageDatabaseException_in1.setIPDatabaseAddress(IPDatabaseAddress)
                                        manageDatabaseException_in1.setIPDatabasePort(IPDatabasePort)
                                        manageDatabaseException_in1.setServiceName(serviceName)
                                        manageDatabaseException_in1.setMySDDAO(mySDDAO)

                                        manageDatabaseException_v2(manageDatabaseException_in1)

                                        //manageDatabaseException(downloadedConfigletID, e, configletsParamsBackupFile, IPDatabaseAddress, IPDatabasePort, serviceName, mySDDAO)
                                    }
                                    totalConfigletsProcessed = totalConfigletsProcessed + 1
                                }
                            }else{
                                //log.info("No existen configlets en el EMS pertenecientes a la solución vbox")
                                log.info("There are no configlets in the EMS belonging to the vbox solution")
                            }
                        }
                        mySDDAO.closeSDConnection()
                    } //Fin args[3] Puerto BBDD SD
                } //Fin args[2] Dirección IP BBDD SD
            }//Fin args[1] Puerto EMS
        } //Fin args[0] Dirección IP EMS
        //log.info("Se ha concluido el procesado de los configlets")
        log.info("The configlet processing has been completed")
        log.info("EXITING. UpdateConfiglets")
    }
}