package util

import com.hp.ov.activator.util.EncryptDecryptCore
import com.hp.ov.activator.util.EncryptDecryptException

@SuppressWarnings("all")
class EncryptDecryptUtil {




    /**
     * Method to decrypt an encrypted string received as a parameter
     * @param paramString String to decrypt
     * @return String decrypted
     */
    static String decrypt(String paramString)
    {
        String aux = ""
        try
        {
            aux = EncryptDecryptCore.decrypt(paramString)
        }
        catch (EncryptDecryptException localEncryptDecryptException) {}
        return aux
    }
}
