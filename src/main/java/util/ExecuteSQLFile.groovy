package util

import oracle.jdbc.driver.OracleDriver
import org.apache.ibatis.jdbc.ScriptRunner
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

import java.sql.Connection
import java.sql.DriverManager

class ExecuteSQLFile {

    private static Logger log = LogManager.getLogger(ExecuteSQLFile.class.getName())
    /**
     * IP address of the internal Oracle Service Director database.<br>
     * This parameter is mandatory.
     */

    private String dirIPBBDD
    /**
     * IP port where the database service is listening in the given IP.<br>
     * This parameter is mandatory.
     */

    private String puertoBBDD
    /**
     * If the database is the local database then the service name is not used. A SID is used instead.<br>
     * If the database is not the local database used to test the functionality
     * then a unique Oracle service name is needed to establish the communication to
     * the database.<br>
     * This parameter is optional.
     */
    private String nombreServicio

    void setDirIPBBDD(String dirIPBBDD) {
        this.dirIPBBDD = dirIPBBDD
    }

    void setPuertoBBDD(String puertoBBDD) {
        this.puertoBBDD = puertoBBDD
    }

    void setNombreServicio(String nombreServicio) {
        this.nombreServicio = nombreServicio
    }

    /**
     * Method executed only when an error occurs when executing an insertion, update, deletion over a vbox database table (configlets or parameters).<br>
     * This method executes a recovery SQL file which was created at the beginning of the execution of the application containing a copy of the existing data
     * of the configlets and parameters table at the moment of the execution.<br>
     * Depending on which action was the cause of the exception it will execute the configlet or the parameters SQL recovery file.<br>
     * @param recoverySQLFile String path of the file where is located the SQL recovery file
     */
    void registerSQLAndExecuteSQLFile(String recoverySQLFile){
        log.info("ENTERING. 'registerSQLAndExecuteSQLFile' method. Class " + ExecuteSQLFile.class.getName())
        //Registering the Driver
        DriverManager.registerDriver(new OracleDriver())
        //Getting the connection
        String oracleUrl = "jdbc:oracle:thin:@" + this.dirIPBBDD +":" + this.puertoBBDD
        Connection con
        if(this.nombreServicio!=null){
            //log.info("Conectando con nombre de servicio " + this.nombreServicio)
            log.info("Connecting with the service " + this.nombreServicio)
            oracleUrl = oracleUrl + "/" + this.nombreServicio
            con = DriverManager.getConnection(oracleUrl,EncryptDecryptUtil.decrypt("xYPq4ICKdytE4mZHryBOtg=="), EncryptDecryptUtil.decrypt("ZWKploRwzHQhoYHw6wOKPw=="))
        }else{
            //log.info("Conectando con SID xe")
            log.info("Connecting with the SID xe")
            oracleUrl = oracleUrl + ":xe"
            con = DriverManager.getConnection(oracleUrl, EncryptDecryptUtil.decrypt("dCqa8VenINxPEaD19KCX5Q=="), EncryptDecryptUtil.decrypt("cr1DYotDW0l2NISQkSevtg=="))
        }
        //log.info("Estableciendo conexión de base de datos con la URL " + oracleUrl)
        log.info("Establishing a database connection over the URL " + oracleUrl)
        if(con!=null){
            //log.info("Se ha establecido la conexión hacia la BBDD en la IP " + this.dirIPBBDD + " y puerto " + this.puertoBBDD)
            log.info("Connection established through the IP " + this.dirIPBBDD + " and IP port " + this.puertoBBDD)
            //Initialize the script runner
            ScriptRunner sr = new ScriptRunner(con)
            String path = ExecuteSQLFile.class.getClassLoader().getResource(recoverySQLFile).toURI().getPath()
            if(path!=null){
                //log.info("Ejecutando archivo SQL " + path)
                log.info("Executing SQL file " + path)
                //Creating a reader object
                Reader reader = new BufferedReader(new FileReader(path))
                //Running the script
                try{
                    sr.runScript(reader)
                }catch(Exception e){
                    log.error(e.printStackTrace())
                    //log.error("Cerrando conexión de Script Runner")
                    log.error("Closing Script Runner connection")
                    sr.closeConnection()
                    con.close()
                    log.info("EXITING. 'registerSQLAndExecuteSQLFile' method. Class " + ExecuteSQLFile.class.getName())
                }
                sr.closeConnection()
            }else{
                //log.error("No se ha podido ejecutar correctamente el archivo por ser nulo el path")
                log.error("The file has not been executed correctly because file path is NULL")
            }
        }else{
            //log.error("No se ha podido establecer correctamente la conexión")
            log.error("The connection couldn't be established correctly")
        }
        //log.info("Cerrando conexión")
        log.info("Closing connection")
        con.close()
        log.info("EXITING. 'registerSQLAndExecuteSQLFile' method. Class " + ExecuteSQLFile.class.getName())
    }
}
