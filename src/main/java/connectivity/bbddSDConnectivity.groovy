package connectivity

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import util.EncryptDecryptUtil

import java.sql.Driver

/**
 * Simple class to establish a <b>unique commutication</b> to the internal Oracle Service Director database.<br>
 * This class is called from the UpdateConfiglets main method at the beginning of the execution
 * to get a connection between the whole application and the Oracle Service Director database.
 * @see main.UpdateConfiglets
 */
@SuppressWarnings("all")
class bbddSDConnectivity {


    /**
     * bbddSDConnectivity class logger
     */
    private static Logger log = LogManager.getLogger(bbddSDConnectivity.class.getName())

    /**
     * Unique connection to the Service Director database in the whole application.<br>
     * This parameter is mandatory.
     */
    static connection

    /**
     * IP address of the internal Oracle Service Director database.<br>
     * This parameter is mandatory.
     */
    private String databaseIPAddress

    /**
     * IP port where the database service is listening in the given IP.<br>
     * This parameter is mandatory.
     */
    private String databaseServicePort

    /**
     * If the database is the local database then the service name is not used. A SID is used instead.<br>
     * If the database is not the local database used to test the functionality
     * then a unique Oracle service name is needed to establish the communication to
     * the database.<br>
     * This parameter is optional.
     */
    private String serviceName

    /**
     * Method to set the database IP address to use
     * @param databaseIPAddress
     */
    void setdatabaseIPAddress(String databaseIPAddress) {
        this.databaseIPAddress = databaseIPAddress
    }

    /**
     * Method to set the database port to use
     * @param databaseServicePort
     */
    void setdatabaseServicePort(String databaseServicePort) {
        this.databaseServicePort = databaseServicePort
    }

    /**
     * Method to set the Oracle service name to use
     * @param serviceName
     */
    void setserviceName(String serviceName) {
        this.serviceName = serviceName
    }

    /**
     * Method to retrieve the Oracle service name established
     * @return Oracle service name of the vbox database
     */
    String getserviceName() {
        return serviceName
    }

    /**
     * Method to retrieve the established IP address of the Oracle database service
     * @return Oracle database IP address
     */
    String getdatabaseIPAddress() {
        return databaseIPAddress
    }

    /**
     * Method to retrieve the established IP port of the Oracle database service
     * @return Oracle database service IP port
     */
    String getdatabaseServicePort() {
        return databaseServicePort
    }

    /**
     * Method to establish a local connection to the <b>local</b> Oracle Service Director database where the needed tables of the vbox solution are located.<br>
     * @return Connection object containing an Oracle connection to the local database or NULL if the connection is not established
     */
    def setToLocalDatabase(){
        log.info("ENTERING. 'setToLocalDatabase' method. Class " + bbddSDConnectivity.class.getName())
        def driver = Class.forName('oracle.jdbc.OracleDriver').newInstance() as Driver
        def props = new Properties()
        props.setProperty("user", EncryptDecryptUtil.decrypt("dCqa8VenINxPEaD19KCX5Q=="))
        props.setProperty("password", EncryptDecryptUtil.decrypt("cr1DYotDW0l2NISQkSevtg=="))
        def conn = driver.connect("jdbc:oracle:thin:@localhost:1521:xe", props)
        if(conn){
            //log.info("Conectado correctamente a la bbdd del SD LOCAL en la IP localhost y puerto 1521")
            log.info("Properly connected to the LOCAL Service Director database on the localhost IP and TCP port 1521")
            connection = conn
        }else{
            //log.error("Error estableciendo la conexion hacia la bbdd del SD LOCAL en la IP localhost y puerto 1521")
            log.error("Error when establishing a connection to the LOCAL Service Director database on localhost IP and TCP port 1521")
        }
        log.info("EXITING. 'setConnection' method. Class " + bbddSDConnectivity.class.getName())
        return connection
    }

    /**
     * Method to establish a connection to the <b>development, preproduction or production</b> Oracle Service Director database where the
     * needed tables of the vbox solution are located.<br>
     * If the connection is established then it is stored in the static property <b>connection</b> of this class.<br>
     * If an error occurs when establishing the connection then a NULL object is returned.<br>
     * @return Connection object containing an Oracle connection to the given database (development, preproduction, production) or NULL if the connection is not established
     */
    def setConnection(){
        log.info("ENTERING. 'setConnection' method. Class " + bbddSDConnectivity.class.getName())
        def driver = Class.forName('oracle.jdbc.OracleDriver').newInstance() as Driver
        def props = new Properties()
        props.setProperty("user", EncryptDecryptUtil.decrypt("xYPq4ICKdytE4mZHryBOtg=="))
        props.setProperty("password", EncryptDecryptUtil.decrypt("ZWKploRwzHQhoYHw6wOKPw=="))
        def conn = driver.connect("jdbc:oracle:thin:@"+this.databaseIPAddress+":" + this.databaseServicePort +"/" + this.serviceName, props)
        if(conn){
            log.info("Properly connected to the LOCAL Service Director database on the " + this.databaseIPAddress + " IP address and TCP port " + this.databaseServicePort)
            connection = conn
        }else{
            //log.error("Error estableciendo la conexion hacia la bbdd del SD en la IP " + this.databaseIPAddress + " y puerto " + this.databaseServicePort)
            log.error("Error establishing a connection to the Service Director database through the IP address " + this.databaseIPAddress + " and TCP port " + this.databaseServicePort)
        }
        log.info("EXITING. 'setConnection' method. Class " + bbddSDConnectivity.class.getName())
        return connection
    }

    /**
     * Method to get the connection previously established in the {setConnection()} method to the <b>development, preproduction or production</b>
     * Oracle Service Director database where the needed tables of the vbox solution are located.<br>
     * If the connection was not established then a NULL object is returned.<br>
     * @return Connection object containing an Oracle connection to the given database (development, preproduction, production) or NULL if the connection is not established
     */
    def getConnection(){
        log.info("ENTERING. 'getConnection' method. Class " + bbddSDConnectivity.class.getName())
        if(connection == null){
            //log.info("No se ha podido recuperar la conexión preestablecida")
            log.info("Could not retrieve preset connection")
            log.info("EXITING. 'getConnection' method. Class " + bbddSDConnectivity.class.getName())
            return null
        }
        //log.info("Obteniendo la conexión preestablecida")
        log.info("Getting the preset connection")
        log.info("EXITING. 'getConnection' method. Class " + bbddSDConnectivity.class.getName())
        return connection
    }
}
